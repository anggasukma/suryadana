<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Clogin';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


# ============================= ROUTING ============================= #

/* AUTH */
	$route['authenticate'] = 'Clogin/login';
	$route['authenticate/logout'] = 'Clogin/logout';

/* DASHBOARD */
	$route['dashboard'] = 'Cdashboard';

/* JADWAL */
	$route['kalender'] = 'ckalender';
	$route['kalender/adddata/(:any)'] = 'Ckegiatan/adddata/$1';

/* PENGGUNA */
$route['user'] = 'Cuser';
$route['user/(:num)'] = 'Cuser/index/$1';
$route['user/listdata'] = 'Cuser/listdata';
$route['user/adddata'] = 'Cuser/adddata';
$route['user/check'] = 'Cuser/check';
$route['user/adddata_action'] = 'Cuser/adddata_action';
$route['user/updatedata/(:num)'] = 'Cuser/updatedata/$1';
$route['user/updatedata_action'] = 'Cuser/updatedata_action';
$route['user/deldata_action'] = 'Cuser/deldata_action';

/* PARAMETER */
	//unit
	$route['unit'] = 'Cunit';
	$route['unit/listdata'] = 'Cunit/listdata';
	$route['unit/adddata'] = 'Cunit/adddata';
	$route['unit/check'] = 'Cunit/check';
	$route['unit/adddata_action'] = 'Cunit/adddata_action';
	$route['unit/updatedata/(:num)'] = 'Cunit/updatedata/$1';
	$route['unit/updatedata_action'] = 'Cunit/updatedata_action';
	$route['unit/deldata_action'] = 'Cunit/deldata_action';
	//alat
	$route['alat'] = 'Calat';
	$route['alat/listdata'] = 'Calat/listdata';
	$route['alat/adddata'] = 'Calat/adddata';
	$route['alat/check'] = 'Calat/check';
	$route['alat/adddata_action'] = 'Calat/adddata_action';
	$route['alat/updatedata/(:num)'] = 'Calat/updatedata/$1';
	$route['alat/updatedata_action'] = 'Calat/updatedata_action';
	$route['alat/deldata_action'] = 'Calat/deldata_action';

/* PENGATURAN */
	$route['pengaturan'] = 'Cpengaturan';
	$route['pengaturan/listdata'] = 'Cpengaturan/listdata';
	$route['pengaturan/updatedata/(:num)'] = 'Cpengaturan/updatedata/$1';
	$route['pengaturan/updatedata_action'] = 'Cpengaturan/updatedata_action';

/* KEGIATAN */
	$route['kegiatan'] = 'Ckegiatan';
	$route['kegiatan/listdata'] = 'Ckegiatan/listdata';
	$route['kegiatan/adddata'] = 'Ckegiatan/adddata';
	$route['kegiatan/adddata_action'] = 'Ckegiatan/adddata_action';
	$route['kegiatan/adddata_action/(:any)'] = 'Ckegiatan/adddata_action/$1';
	$route['kegiatan/updatedata/(:num)'] = 'Ckegiatan/updatedata/$1';
	$route['kegiatan/updatedata_action'] = 'Ckegiatan/updatedata_action';
	$route['kegiatan/deldata_action'] = 'Ckegiatan/deldata_action';
	$route['kegiatan/alat/(:num)'] = 'Ckegiatan/alat/$1';
	$route['kegiatan/addalat_action'] = 'Ckegiatan/addalat_action';
	$route['kegiatan/listalat'] = 'Ckegiatan/listalat';
	$route['kegiatan/delalat_action'] = 'Ckegiatan/delalat_action';
	$route['kegiatan/listpeminjamanalat'] = 'Ckegiatan/listpeminjamanalat';
	$route['kegiatansaya'] = 'Ckegiatan';
	$route['kegiatansaya/alat/(:num)'] = 'Ckegiatan/alat/$1';
	$route['kegiatansaya/prosespengerjaan_action'] = 'Ckegiatan/prosespengerjaan_action';
	$route['kegiatan/prosespengerjaan_action'] = 'Ckegiatan/prosespengerjaan_action';
	$route['kegiatan/pinjamalat_action'] = 'Ckegiatan/pinjamalat_action';
	$route['kegiatan/kembalialat_action'] = 'Ckegiatan/kembalialat_action';
	$route['kegiatan/cekbarangkembali'] = 'Ckegiatan/cekbarangkembali';