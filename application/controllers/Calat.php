<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calat extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/alat_guide/general/urls.html
	 */

	# global construct #
	public function __construct()
    {
     	parent::__construct();  
		$akses=array("Admin");
        ceklogin($akses);
        $this->load->model('Malat');
	}


	public function index()
	{
		$this->layout->display('alat/valat');
	}


	public function listdata()
	{
		$data = $this->Malat->getListData($this->input->post());
		$datacount = $this->Malat->getListData($this->input->post(),'count');

		if($data != null){
			foreach ($data as $key => $value) {
				$data[$key]['aksi'] = '
					<button class="btn btn-xs btn-primary" title="Edit Data" onclick="update('.$value['idalat'].');"><i class="fa fa-pencil"></i></button>
					<button class="btn btn-xs btn-danger" title="Hapus Data" onclick="del(\''.$value['idalat'].'\',\''.$value['kodealat'].'\');"><i class="fa fa-trash"></i></button>
				';		
			}
		} else {$data = array();}

		$results = array(
			"iTotalRecords" => count($datacount),
			"iTotalDisplayRecords" => count($datacount),
			"aaData"=>$data
		);
		echo json_encode($results);

	}

	function adddata()
	{
		$this->layout->display('alat/valatadd');
	}
	function updatedata()
	{
		$idalat=$this->uri->segment(app_param());

		$conditions = array("idalat" => $idalat);
		$data['dataalat']=$this->Malat->getWhere($conditions);

		$this->layout->display('alat/valatupdate',$data);
	}

	public function check(){
		if($this->input->post('kodealat') != ''){

			$postcheck = array("kodealat" => $this->input->post('kodealat'));
			if($this->input->post('idalat') != ''){ $postcheck['idalat != '] = $this->input->post('idalat'); }

			$checkresult = $this->Malat->getWhere($postcheck);
			if(count($checkresult) > 0){
				echo json_encode(false);
			} else {
				echo json_encode(true);
			}
		}
	}

	public function adddata_action(){
		$post = $this->input->post();

		$aksi = $this->Malat->store($post);
		if($aksi){
			$this->session->set_flashdata('success', word_modal('success_add'));
		} else {
			$this->session->set_flashdata('error', word_modal('failed_add'));
		}
		redirect('alat');
		
	}
	public function updatedata_action(){
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;

		$idalat = $this->input->post('idalat');
		$post = $this->input->post();

		$aksi = $this->Malat->update($idalat,$post);
		if($aksi){
			$this->session->set_flashdata('success', word_modal('success_add'));
		} else {
			$this->session->set_flashdata('error', word_modal('failed_add'));
		}
		redirect('alat');
		
	}
	public function deldata_action(){
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;
		
		$idalat = $this->input->post('idalat_del');

		$aksi = $this->Malat->delete($idalat);
		if($aksi){
			$this->session->set_flashdata('success', word_modal('success_del'));
		} else {
			$this->session->set_flashdata('error', word_modal('failed_del'));
		}

		//$this->output->enable_profiler('true');
		redirect('alat');
		
	}

}
