<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ckalender extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	# global construct #
	public function __construct()
    {
     	parent::__construct();  
		$akses=array("Admin","Petugas IT");
        ceklogin($akses);
        $this->load->model('Mkegiatan');
	}


	public function index()
	{
		$datakegiatan=$this->Mkegiatan->getData();
		// print_r($datakegiatan);

		$event='';
		if($datakegiatan !=null) {
			foreach($datakegiatan as $r):	
				if($event!=""){
					$event.=",";
				}			
				$idkegiatan=$r->idkegiatan;
				$tglkegiatan=$r->tglkegiatan;
					$tglkegiatan_ex=explode(" ",$tglkegiatan);
					$datekegiatan=$tglkegiatan_ex[0];
						$datekegiatan_ex=explode("-",$datekegiatan);
						$tgl=$datekegiatan_ex[2];
						$bln=$datekegiatan_ex[1]-1;
						$thn=$datekegiatan_ex[0];
					$timekegiatan=$tglkegiatan_ex[1];
				$namakegiatan=$r->namakegiatan;
				$namakegiatan_t = (strlen($namakegiatan) > 25) ? substr($namakegiatan,0,25).'...' : $namakegiatan;
				$lokasikegiatan=$r->lokasikegiatan;
				$idunit=$r->idunit;
				$picunitkegiatan=$r->picunitkegiatan;
				$ketkegiatan=$r->ketkegiatan;
				$iduser_petugasit=$r->iduser_petugasit;
				$statuskegiatan=$r->statuskegiatan;

				$event.="
				{
					title: '".$timekegiatan." | ".$namakegiatan_t."',
					start: new Date(".$thn.", ".$bln.", ".$tgl."),
					url: '".app_path('kegiatan/alat/'.$idkegiatan)."'
				}
				";
			endforeach;
		}

		// echo $event;
		$data['event']=$event;

		$this->layout->display('kalender/vkalender',$data);
	}

}
