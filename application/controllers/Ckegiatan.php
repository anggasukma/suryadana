<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ckegiatan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/kegiatan_guide/general/urls.html
	 */

	# global construct #
	public function __construct()
    {
     	parent::__construct();  
		$akses=array("Admin","Petugas IT");
        ceklogin($akses);
        $this->load->model('Mkegiatan');
	}


	public function index()
	{
		$this->layout->display('kegiatan/vkegiatan');
	}


	public function listdata()
	{
		$data = $this->Mkegiatan->getListData($this->input->post());
		$datacount = $this->Mkegiatan->getListData($this->input->post(),'count');

		if($data != null){
			foreach ($data as $key => $value) {
				$data[$key]['tglkegiatan_t']= '<font color='.warna_statuskegiatan($value['statuskegiatan']).'>'.$value['statuskegiatan'].'</font>';
				$data[$key]['aksi']= '';

				if($this->session->userdata('jadwal_user_data')->hakakses=="Admin" && $value['statuskegiatan']!="Selesai"){
					$data[$key]['aksi'].= '
					<button class="btn btn-xs btn-primary" title="Edit Data" onclick="update('.$value['idkegiatan'].');"><i class="fa fa-pencil"></i></button>
					';		
				}
				$data[$key]['aksi'].= '
					<button class="btn btn-xs btn-success" title="Pengaturan Kegiatan & Alat" onclick="alat(\''.$value['idkegiatan'].'\',\''.$value['namakegiatan'].'\');"><i class="fa fa-cog"></i></button>
				';
				if($this->session->userdata('jadwal_user_data')->hakakses=="Admin" && $value['statuskegiatan']!="Selesai"){
					$data[$key]['aksi'].= '
					<button class="btn btn-xs btn-danger" title="Hapus Data" onclick="del(\''.$value['idkegiatan'].'\',\''.$value['namakegiatan'].'\');"><i class="fa fa-trash"></i></button>
					';		
				}

				if($value['iduser_petugasit']==$this->session->userdata('jadwal_user_id') || $this->session->userdata('jadwal_user_data')->hakakses=="Admin"){
					//untuk tampilan aksi petugas IT terkait
					if($value['statuskegiatan']=="Belum Dikerjakan"){
						$data[$key]['aksi'].= '
							<button class="btn btn-xs btn-primary" title="Mulai Kerjakan" onclick="prosespengerjaan(\'Sedang Dikerjakan\',\'tglsedangdikerjakan\',\''.$value['idkegiatan'].'\',\''.$value['namakegiatan'].'\');"><i class="fa fa-check-circle"></i> Mulai Kerjakan</button>
						';
					} else if($value['statuskegiatan']=="Sedang Dikerjakan"){
						$data[$key]['aksi'].= '
							<button class="btn btn-xs btn-primary" title="Sudah Dikerjakan" onclick="prosespengerjaan(\'Sudah Dikerjakan\',\'tglsudahdikerjakan\',\''.$value['idkegiatan'].'\',\''.$value['namakegiatan'].'\');"><i class="fa fa-check-circle"></i> Sudah Dikerjakan</button>
						';
					} else if($value['statuskegiatan']=="Sudah Dikerjakan"){
						$data[$key]['aksi'].= '
							<button class="btn btn-xs btn-primary" title="Selesai" onclick="prosespengerjaan(\'Selesai\',\'tglselesai\',\''.$value['idkegiatan'].'\',\''.$value['namakegiatan'].'\');"><i class="fa fa-flag"></i> Selesai</button>
						';
					} else {
						//pekerjaan selesai tidak muncul apa2
					}
				}
			}
		} else {$data = array();}

		$results = array(
			"iTotalRecords" => count($datacount),
			"iTotalDisplayRecords" => count($datacount),
			"aaData"=>$data
		);
		echo json_encode($results);

	}

	function adddata()
	{
		$tglkegiatan=$this->uri->segment(app_param());
		if($tglkegiatan==""){
			$tglkegiatan=get_datetime('2');
		}
		$data['tglkegiatan']=$tglkegiatan;
		$this->layout->display('kegiatan/vkegiatanadd',$data);
	}
	function updatedata()
	{
		$idkegiatan=$this->uri->segment(app_param());

		$conditions = array("idkegiatan" => $idkegiatan);
		$data['datakegiatan']=$this->Mkegiatan->getWhere($conditions);

		$this->layout->display('kegiatan/vkegiatanupdate',$data);
	}
	function alat()
	{
		$idkegiatan=$this->uri->segment(app_param());

		$conditions = array("idkegiatan" => $idkegiatan);
		$data['datakegiatan']=$this->Mkegiatan->getWhere($conditions);

		$this->layout->display('kegiatan/vkegiatanalat',$data);
	}

	public function adddata_action(){
		$act=$this->uri->segment(app_param());

		$post = $this->input->post();
		$post['iduser_admin']=$this->session->userdata('jadwal_user_id');
		$post['statuskegiatan']="Belum Dikerjakan";
		$post['tglinput']=get_datetime('5');
		$post['tglkegiatan']=$post['datekegiatan']." ".$post['timekegiatan'];
		unset($post['datekegiatan']);
		unset($post['timekegiatan']);

		$aksi = $this->Mkegiatan->store($post);
		if($aksi){
			$this->session->set_flashdata('success', word_modal('success_add'));
		} else {
			$this->session->set_flashdata('error', word_modal('failed_add'));
		}

		if($act=="kalender"){
			redirect('kalender');
		} else {
			redirect('kegiatan');
		}
		
	}
	public function updatedata_action(){
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;

		$idkegiatan = $this->input->post('idkegiatan');
		$post = $this->input->post();
		$post['tglkegiatan']=$post['datekegiatan']." ".$post['timekegiatan'];
		unset($post['datekegiatan']);
		unset($post['timekegiatan']);

		$aksi = $this->Mkegiatan->update($idkegiatan,$post);
		if($aksi){
			// echo "ok";
			$this->session->set_flashdata('success', word_modal('success_add'));
		} else {
			// echo "gagal";
			$this->session->set_flashdata('error', word_modal('failed_add'));
		}
		redirect('kegiatan');
		
	}
	public function prosespengerjaan_action(){
		$s=$this->uri->segment(1);
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;

		$idkegiatan = $this->input->post('idkegiatan');
		$post = $this->input->post();
		$post[$post['titletgledit']]=$post['datekegiatan']." ".$post['timekegiatan'];
		unset($post['titletgledit']);
		unset($post['datekegiatan']);
		unset($post['timekegiatan']);

		// print_r($post);

		$aksi = $this->Mkegiatan->updateprosespengerjaan($idkegiatan,$post);
		if($aksi){
			// echo "ok";
			$this->session->set_flashdata('success', word_modal('success_add'));
		} else {
			// echo "gagal";
			$this->session->set_flashdata('error', word_modal('failed_add'));
		}
		redirect($s);
		// redirect('kegiatansaya');
		// $this->output->enable_profiler('true');
		
	}
	public function cekbarangkembali(){
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;

		$idkegiatan = $this->input->post('idkegiatan');

		// print_r($post);
		$conditions = array("idkegiatan" => $idkegiatan,"tglkembali" => "0000-00-00 00:00:00");
		$data= $this->Mkegiatan->getWhereRowsAlat($conditions);
		
		echo $data;
		// $this->output->enable_profiler('true');
		
	}
	public function deldata_action(){
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;
		
		$idkegiatan = $this->input->post('idkegiatan_del');

		$aksi = $this->Mkegiatan->delete($idkegiatan);
		if($aksi){
			$this->session->set_flashdata('success', word_modal('success_del'));
		} else {
			$this->session->set_flashdata('error', word_modal('failed_del'));
		}

		//$this->output->enable_profiler('true');
		redirect('kegiatan');
		
	}


/* KEGIATAN ALAT */
	public function addalat_action(){
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;
		
		$post = $this->input->post();

		$aksi = $this->Mkegiatan->storealat($post);
		if($aksi){
			$this->session->set_flashdata('success', word_modal('success_add'));
		} else {
			$this->session->set_flashdata('error', word_modal('failed_add'));
		}
		redirect('kegiatan/alat/'.$post['idkegiatan']);
		
	}

	public function pinjamalat_action(){
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;

		$idkegiatanalat = $this->input->post('idkegiatanalat');
		$idkegiatan = $this->input->post('idkegiatan');
		$post = $this->input->post();
		$post['tglpinjam']=$post['datepinjam']." ".$post['timepinjam'];
		unset($post['datepinjam']);
		unset($post['timepinjam']);
		unset($post['idkegiatan']);

		$aksi = $this->Mkegiatan->updatealat($idkegiatanalat,$post);
		if($aksi){
			// echo "ok";
			$this->session->set_flashdata('success', word_modal('success_add'));
		} else {
			// echo "gagal";
			$this->session->set_flashdata('error', word_modal('failed_add'));
		}
		redirect('kegiatan/alat/'.$idkegiatan);
		
	}
	public function kembalialat_action(){
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;

		$idkegiatanalat = $this->input->post('idkegiatanalat');
		$idkegiatan = $this->input->post('idkegiatan');
		$post = $this->input->post();
		$post['tglkembali']=$post['datekembali']." ".$post['timekembali'];
		unset($post['datekembali']);
		unset($post['timekembali']);
		unset($post['idkegiatan']);

		$aksi = $this->Mkegiatan->updatealat($idkegiatanalat,$post);
		if($aksi){
			// echo "ok";
			$this->session->set_flashdata('success', word_modal('success_add'));
		} else {
			// echo "gagal";
			$this->session->set_flashdata('error', word_modal('failed_add'));
		}
		redirect('kegiatan/alat/'.$idkegiatan);
		
	}

	public function listalat()
	{
		$data = $this->Mkegiatan->getListAlat($this->input->post());
		$datacount = $this->Mkegiatan->getListAlat($this->input->post(),'count');

		if($data != null){
			foreach ($data as $key => $value) {
				$data[$key]['aksi'] = '';

				if($this->session->userdata('jadwal_user_data')->hakakses=="Admin" && $value['statuskegiatan']!="Selesai"){
					if($value['tglpinjam']=="0000-00-00 00:00:00"){
						$data[$key]['aksi'].= '
							<button class="btn btn-xs btn-warning" title="Alat Pinjam" onclick="pinjam(\''.$value['idkegiatan'].'\',\''.$value['idkegiatanalat'].'\',\''.$value['namaalat'].'\',\''.$value['kodealat'].'\');"><i class="fa fa-upload"> Pinjam</i></button>
						';
					} else {
						if($value['tglkembali']=="0000-00-00 00:00:00"){
							$data[$key]['aksi'].= '
								<button class="btn btn-xs btn-success" title="Alat Kembali" onclick="kembali(\''.$value['idkegiatan'].'\',\''.$value['idkegiatanalat'].'\',\''.$value['namaalat'].'\',\''.$value['kodealat'].'\');"><i class="fa fa-download"> Kembali</i></button>
							';
						} else {
							//barang sdh dipinjam dan sudah kembali
						}
					}
					$data[$key]['aksi'].= '
						<button class="btn btn-xs btn-danger" title="Hapus Alat" onclick="delalat(\''.$value['idkegiatan'].'\',\''.$value['idkegiatanalat'].'\',\''.$value['namaalat'].'\');"><i class="fa fa-trash"></i></button>
					';
				}

				if($value['iduser_petugasit']==$this->session->userdata('jadwal_user_id')){
					//untuk tampilan aksi petugas IT terkait
				}
			}
		} else {$data = array();}

		$results = array(
			"iTotalRecords" => count($datacount),
			"iTotalDisplayRecords" => count($datacount),
			"aaData"=>$data
		);
		echo json_encode($results);

	}
	public function listpeminjamanalat()
	{
		$data = $this->Mkegiatan->getListPAlat($this->input->post());
		$datacount = $this->Mkegiatan->getListPAlat($this->input->post(),'count');

		if($data != null){
			foreach ($data as $key => $value) {
			}
		} else {$data = array();}

		$results = array(
			"iTotalRecords" => count($datacount),
			"iTotalDisplayRecords" => count($datacount),
			"aaData"=>$data
		);
		echo json_encode($results);
		// $this->output->enable_profiler(TRUE);
	}
	public function delalat_action(){
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;
		
		$idkegiatanalat = $this->input->post('idkegiatanalat_del');

		$aksi = $this->Mkegiatan->deletealat($idkegiatanalat);
		if($aksi){
			$this->session->set_flashdata('success', word_modal('success_del'));
		} else {
			$this->session->set_flashdata('error', word_modal('failed_del'));
		}

		//$this->output->enable_profiler('true');
		redirect('kegiatan/alat/'.$this->input->post('idkegiatan_del'));
		
	}
/* END KEGIATAN ALAT */
}
