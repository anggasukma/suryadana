<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clogin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	# global construct #
	public function __construct()
    {
     	parent::__construct();  
        $this->load->model('Muser');
	}


	public function index()
	{
		if(empty($this->session->userdata('jadwal_user_id'))){
			$this->layout->displayblank('auth/vlogin');
		} else {
			redirect(app_path('dashboard'));
		}		
	}


	/*
	* fungsi login
	*/
	public function login(){
		/* cek user */
		$kondisi = array(
			"username" => $this->input->post('username'),
			"password" => $this->input->post('password')
		);
		$cekuser = $this->Muser->getWhere($kondisi);
		$redirectpath="";
		if(count($cekuser) > 0){

			/* set session */
			$this->set_session($cekuser[0]);

			$this->session->set_flashdata('info', 'Selamat Datang, '.$this->session->userdata('jadwal_user_data')->namauser);
			$redirectpath="dashboard";
		} else {
			$this->session->set_flashdata('error', 'Data user tidak ditemukan');
		}
		redirect(app_path($redirectpath));
	}


	/* set session */
	public function set_session($datauser)
	{
		if($datauser != null){
			$this->session->jadwal_user_id = $datauser->iduser;
			$this->session->jadwal_user_data = $datauser;
		}
	}

	public function logout(){
		session_destroy();
		redirect(app_path('/'));
	}



}
