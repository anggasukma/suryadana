<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cpengaturan extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/pengaturan_guide/general/urls.html
	 */

	# global construct #
	public function __construct()
    {
     	parent::__construct();  
		$akses=array("Admin");
        ceklogin($akses);
        $this->load->model('Mpengaturan');
	}


	public function index()
	{
		$this->layout->display('pengaturan/vpengaturan');
	}


	public function listdata()
	{
		$data = $this->Mpengaturan->getListData($this->input->post());
		$datacount = $this->Mpengaturan->getListData($this->input->post(),'count');

		if($data != null){
			foreach ($data as $key => $value) {
				$data[$key]['aksi'] = '
					<button class="btn btn-xs btn-primary" title="Edit Data" onclick="update('.$value['iddata'].');"><i class="fa fa-pencil"></i></button>
				';				
			}
		} else {$data = array();}

		$results = array(
			"iTotalRecords" => count($datacount),
			"iTotalDisplayRecords" => count($datacount),
			"aaData"=>$data
		);
		echo json_encode($results);

	}

	function updatedata()
	{
		$iddata=$this->uri->segment(app_param());

		$conditions = array("iddata" => $iddata);
		$data['datapengaturan']=$this->Mpengaturan->getWhere($conditions);

		$this->layout->display('pengaturan/vpengaturanupdate',$data);
	}

	public function updatedata_action(){
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;

		$iddata = $this->input->post('iddata');
		$post = $this->input->post();

		$aksi = $this->Mpengaturan->update($iddata,$post);
		if($aksi){
			$this->session->set_flashdata('success', word_modal('success_add'));
		} else {
			$this->session->set_flashdata('error', word_modal('failed_add'));
		}
		redirect('pengaturan');
		
	}

}
