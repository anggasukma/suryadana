<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cunit extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/unit_guide/general/urls.html
	 */

	# global construct #
	public function __construct()
    {
     	parent::__construct();  
		$akses=array("Admin");
        ceklogin($akses);
        $this->load->model('Munit');
	}


	public function index()
	{
		$this->layout->display('unit/vunit');
	}


	public function listdata()
	{
		$data = $this->Munit->getListData($this->input->post());
		$datacount = $this->Munit->getListData($this->input->post(),'count');

		if($data != null){
			foreach ($data as $key => $value) {
				$data[$key]['aksi'] = '
					<button class="btn btn-xs btn-primary" title="Edit Data" onclick="update('.$value['idunit'].');"><i class="fa fa-pencil"></i></button>
					<button class="btn btn-xs btn-danger" title="Hapus Data" onclick="del(\''.$value['idunit'].'\',\''.$value['namaunit'].'\');"><i class="fa fa-trash"></i></button>
				';		
			}
		} else {$data = array();}

		$results = array(
			"iTotalRecords" => count($datacount),
			"iTotalDisplayRecords" => count($datacount),
			"aaData"=>$data
		);
		echo json_encode($results);

	}

	function adddata()
	{
		$this->layout->display('unit/vunitadd');
	}
	function updatedata()
	{
		$idunit=$this->uri->segment(app_param());

		$conditions = array("idunit" => $idunit);
		$data['dataunit']=$this->Munit->getWhere($conditions);

		$this->layout->display('unit/vunitupdate',$data);
	}

	public function check(){
		if($this->input->post('namaunit') != ''){

			$postcheck = array("namaunit" => $this->input->post('namaunit'));
			if($this->input->post('idunit') != ''){ $postcheck['idunit != '] = $this->input->post('idunit'); }

			$checkresult = $this->Munit->getWhere($postcheck);
			if(count($checkresult) > 0){
				echo json_encode(false);
			} else {
				echo json_encode(true);
			}
		}
	}

	public function adddata_action(){
		$post = $this->input->post();

		$aksi = $this->Munit->store($post);
		if($aksi){
			$this->session->set_flashdata('success', word_modal('success_add'));
		} else {
			$this->session->set_flashdata('error', word_modal('failed_add'));
		}
		redirect('unit');
		
	}
	public function updatedata_action(){
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;

		$idunit = $this->input->post('idunit');
		$post = $this->input->post();

		$aksi = $this->Munit->update($idunit,$post);
		if($aksi){
			$this->session->set_flashdata('success', word_modal('success_add'));
		} else {
			$this->session->set_flashdata('error', word_modal('failed_add'));
		}
		redirect('unit');
		
	}
	public function deldata_action(){
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;
		
		$idunit = $this->input->post('idunit_del');

		$aksi = $this->Munit->delete($idunit);
		if($aksi){
			$this->session->set_flashdata('success', word_modal('success_del'));
		} else {
			$this->session->set_flashdata('error', word_modal('failed_del'));
		}

		//$this->output->enable_profiler('true');
		redirect('unit');
		
	}

}
