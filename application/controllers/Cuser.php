<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cuser extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	# global construct #
	public function __construct()
    {
     	parent::__construct();  
		$akses=array("Admin");
        ceklogin($akses);
        $this->load->model('Muser');
	}


	public function index()
	{
		$this->layout->display('user/vuser');
	}


	public function listdata()
	{
		$data = $this->Muser->getListData($this->input->post());
		$datacount = $this->Muser->getListData($this->input->post(),'count');

		if($data != null){
			foreach ($data as $key => $value) {
				$data[$key]['aksi'] = '
					<button class="btn btn-xs btn-primary" title="Edit Data" onclick="update('.$value['iduser'].');"><i class="fa fa-pencil"></i></button>
					<button class="btn btn-xs btn-danger" title="Hapus Data" onclick="del(\''.$value['iduser'].'\',\''.$value['username'].'\');"><i class="fa fa-trash"></i></button>
				';				
				$data[$key]['statususer_t'] = word_status($value['statususer']);				
			}
		} else {$data = array();}

		$results = array(
			"iTotalRecords" => count($datacount),
			"iTotalDisplayRecords" => count($datacount),
			"aaData"=>$data
		);
		echo json_encode($results);
		//$this->output->enable_profiler('true');
	}

	function adddata()
	{
		$this->layout->display('user/vuseradd');
	}
	function updatedata()
	{
		$iduser=$this->uri->segment(app_param());

		$conditions = array("iduser" => $iduser);
		$data['datauser']=$this->Muser->getWhere($conditions);

		$this->layout->display('user/vuserupdate',$data);
	}

	public function check(){
		if($this->input->post('username') != ''){

			$postcheck = array("username" => $this->input->post('username'));
			if($this->input->post('iduser') != ''){ $postcheck['iduser != '] = $this->input->post('iduser'); }

			$checkresult = $this->Muser->getWhere($postcheck);
			if(count($checkresult) > 0){
				echo json_encode(false);
			} else {
				echo json_encode(true);
			}
		}
	}

	public function adddata_action(){
		$post = $this->input->post();
		$post['create_at'] = get_datetime('5');

		$aksi = $this->Muser->store($post);
		if($aksi){
			$this->session->set_flashdata('success', word_modal('success_add'));
		} else {
			$this->session->set_flashdata('error', word_modal('failed_add'));
		}
		redirect('user');
		
	}
	public function updatedata_action(){
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;
		
		$iduser = $this->input->post('iduser');
		$password = $this->input->post('password');
		$dataupdate['namauser'] = $this->input->post('namauser');
		$dataupdate['hakakses'] = $this->input->post('hakakses');
		$dataupdate['statususer'] = $this->input->post('statususer');
		if($password!=""){
			$dataupdate['password'] = $password;
		}

		$aksi = $this->Muser->update($iduser,$dataupdate);
		if($aksi){
			$this->session->set_flashdata('success', word_modal('success_add'));
		} else {
			$this->session->set_flashdata('error', word_modal('failed_add'));
		}
		redirect('user');
		
	}
	public function deldata_action(){
		$db_debug = $this->db->db_debug;
		$this->db->db_debug = false;

		$iduser = $this->input->post('iduser_del');

		$aksi = $this->Muser->delete($iduser);
		if($aksi){
			$this->session->set_flashdata('success', word_modal('success_del'));
		} else {
			$this->session->set_flashdata('error', word_modal('failed_del'));
		}

		//$this->output->enable_profiler('true');
		redirect('user');
		
	}

}
