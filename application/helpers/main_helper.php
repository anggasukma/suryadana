<?php
/* =====================================================================*/
/* MAIN HELPER :: AUTOLOAD 												*/
/*======================================================================*/

/* ||	checking login || */
function ceklogin($akses=null){	
	$ci = get_instance();
	if(!empty($ci->session->userdata('jadwal_user_id')) && (in_array($ci->session->jadwal_user_data->hakakses, $akses)) ){
		return true;
	} else {
		redirect(app_path('authenticate/logout'));
	}
}

function docroot(){
	return 'penjadwalan';
}

function docroot_logo(){
	return $_SERVER['DOCUMENT_ROOT'].'/'.docroot().'/assets/images/logojpg.jpg';
}
function app_controller(){
	return "1";
}
function app_paramview(){
	return "2";
}
function app_param(){
	return "3";
}
function app_path($c){
	return site_url($c);
	//return base_url($c);
}
function app_titlept(){	
	return get_datapengaturan(1);
}
function app_title(){	
	return get_datapengaturan(1);
}
function app_titledashboard(){	
	return get_datapengaturan(2);
}
function app_logo(){	
	return "logo.png";
}

function warna_statuskegiatan($statuskegiatan){
	switch ($statuskegiatan) {
		case "Belum Dikerjakan":
			$dtreturn='red';
		break;
		case "Sedang Dikerjakan":
			$dtreturn='orange';
		break;
		case "Sudah Dikerjakan":
			$dtreturn='green';
		break;
		case "Selesai":
			$dtreturn='blue';
		break;
  	}	
	return $dtreturn;
}

function get_datapengaturan($id){
	$dtreturn='';
	
	$CI =& get_instance();
	$CI->load->model('Mpengaturan');
	$param = array( "iddata" => $id );
	$datapengaturan=$CI->Mpengaturan->getWhere($param);
	if($datapengaturan !=null) {
		foreach($datapengaturan as $r):
			$dtreturn=$r->isidata;
		endforeach;
	}
	
  return $dtreturn;
}

/* || get enum from field table || */
function word_status($status){
	/*
	yang menggunakan :
	- tb_user->statususer
	*/
	$dtreturn='';
	switch ($status) {
		case "0":
			$dtreturn='Tidak Aktif';
		break;
		case "1":
			$dtreturn='Aktif';
		break;
  }
  return $dtreturn;
}

function get_selectoption_tb_user($field){
	$dtreturn='';
	switch ($field) {
		case "hakakses":
			$dtreturn='
				<option value="Admin">Admin</option>
				<option value="Petugas IT">Petugas IT</option>
			';
		break;
		case "statususer":
			$dtreturn='
				<option value="1">'.word_status('1').'</option>
				<option value="0">'.word_status('0').'</option>
			';
		break;
		case "datapetugasit":
			$CI =& get_instance();
			$kondisi['hakakses']="Petugas IT";
			$kondisi['statususer']="1";
			$CI->load->model('Muser');
			$selectdata=$CI->Muser->getWhere($kondisi);
			$dtreturn.='<option value="">---</option>';
			if($selectdata !=null) {
				foreach($selectdata as $r):
					$dtreturn.='<option value="'.$r->iduser.'">'.$r->namauser.'</option>';
				endforeach;
			}
		break;
  }
  return $dtreturn;
}

function get_selectoption_tb_kegiatan($field){
	$dtreturn='';
	switch ($field) {
		case "statuskegiatan":
			$dtreturn='
				<option value="">---</option>
				<option value="Belum Dikerjakan">Belum Dikerjakan</option>
				<option value="Sedang Dikerjakan">Sedang Dikerjakan</option>
				<option value="Sudah Dikerjakan">Sudah Dikerjakan</option>
				<option value="Selesai">Selesai</option>
			';
		break;
  }
  return $dtreturn;
}

function get_selectoption_tb_unit($field){
	$dtreturn='';
	switch ($field) {
		case "data":
			$CI =& get_instance();
			$CI->load->model('Munit');
			$selectdata=$CI->Munit->getData();
			$dtreturn.='<option value="">---</option>';
			if($selectdata !=null) {
				foreach($selectdata as $r):
					$dtreturn.='<option value="'.$r->idunit.'">'.$r->namaunit.'</option>';
				endforeach;
			}
		break;
  }
  return $dtreturn;
}

function get_selectoption_tb_alat($field){
	$dtreturn='';
	switch ($field) {
		case "data":
			$CI =& get_instance();
			$CI->load->model('Malat');
			$selectdata=$CI->Malat->getData();
			$dtreturn.='<option value="">---</option>';
			if($selectdata !=null) {
				foreach($selectdata as $r):
					$dtreturn.='<option value="'.$r->idalat.'">'.$r->kodealat.' | '.$r->namaalat.'</option>';
				endforeach;
			}
		break;
  }
  return $dtreturn;
}

/* || get kata2 pemberitahuan || */
function word_modal($kata){
	$dtreturn='';
	switch ($kata) {
		case "success_add":
			$dtreturn='Berhasil tersimpan.';
		break;
		case "failed_add":
			$dtreturn='Gagal tersimpan.';
		break;
		case "success_del":
			$dtreturn='Berhasil terhapus.';
		break;
		case "failed_del":
			$dtreturn='Gagal terhapus. Kemungkinan data telah digunakan.';
		break;
  }
  return $dtreturn;
}

/* || for get date || */
function get_datetime($param,$b=null,$a=null){
	$CI =& get_instance();
	$CI->load->model('Mhelper');
	$datareturn="Tidak ada data";
	$data=null;
	
	switch ($param) {
		case "1":
		//190121
		$data=$CI->Mhelper->get_date();
		break;
		case "2":
		//2019-01-21
		$data=$CI->Mhelper->get_date2();
		break;
		case "2min":
		//2019-01-20
		$data=$CI->Mhelper->get_date2_mindate($b);
		break;
		case "2plus":
		//2019-01-22
		$data=$CI->Mhelper->get_date2_plusdate($b,$a);
		break;
		case "2awal":
		//2019-01-20
			$tgl_ex=explode("-",$b);
			$tgl_ex_tgl=$tgl_ex[2];
			$tgl_ex_bln=$tgl_ex[1];
			$tgl_ex_thn=$tgl_ex[0];
			$tglawal=$tgl_ex_thn."-".$tgl_ex_bln."-01";
			$datareturn=$tglawal;
		break;
		case "2akhir":
		//2019-01-20
		$data=$CI->Mhelper->get_date2_akhirdate($b);
		break;
		case "3":
		//133322
		$data=$CI->Mhelper->get_time();
		break;
		case "4":
		//13:33:36
		$data=$CI->Mhelper->get_time2();
		break;
		case "5":
		//2019-01-21 13:34:21
		$data=$CI->Mhelper->get_datetime();
		break;
		case "6":
		//20190121133412
		$data=$CI->Mhelper->get_datetime2();
		break;
		case "7":
		//1902
		$data=$CI->Mhelper->get_date3();
		break;
  }

  	if($data){
		$datareturn=$data[0]->datareturn;
	}
	
	return $datareturn;
}

/* || gari tanggal bulan || */
function CariHari($ttggll=''){
	//$tgldpt=explode("/",$ttggll);
	//$extgl=$tgldpt[0];
	//$exbln=$tgldpt[1];
	//$exth=$tgldpt[2];
	$tglcarihari=$ttggll;
	$tanggalcarihari = strtotime($tglcarihari);
	$hari_en = date('l', $tanggalcarihari);
	$hari_ar = array("Monday"=>"Senin", "Tuesday"=>"Selasa", "Wednesday"=>"Rabu", "Thursday"=>"Kamis", "Friday"=>"Jumat", "Saturday"=>"Sabtu", "Sunday"=>"Minggu");
	$hari_id = $hari_ar[$hari_en];
	return($hari_id);
}	
function namabulan($b=''){
	//buat nm bln
				if($b=="01"){
					$nmbln="Januari";
				} else if($b=="02"){
					$nmbln="Februari";
				} else if($b=="03"){
					$nmbln="Maret";
				} else if($b=="04"){
					$nmbln="April";
				} else if($b=="05"){
					$nmbln="Mei";
				} else if($b=="06"){
					$nmbln="Juni";
				} else if($b=="07"){
					$nmbln="Juli";
				} else if($b=="08"){
					$nmbln="Agustus";
				} else if($b=="09"){
					$nmbln="September";
				} else if($b=="10"){
					$nmbln="Oktober";
				} else if($b=="11"){
					$nmbln="Nopember";
				} else if($b=="12"){
					$nmbln="Desember";
				} else {
					$nmbln="";
				}
	return($nmbln);
}	
function namabulan2($b=''){
	//buat nm bln
				if($b=="01"){
					$nmbln="Jan";
				} else if($b=="02"){
					$nmbln="Feb";
				} else if($b=="03"){
					$nmbln="Mar";
				} else if($b=="04"){
					$nmbln="Apr";
				} else if($b=="05"){
					$nmbln="Mei";
				} else if($b=="06"){
					$nmbln="Jun";
				} else if($b=="07"){
					$nmbln="Jul";
				} else if($b=="08"){
					$nmbln="Agst";
				} else if($b=="09"){
					$nmbln="Sep";
				} else if($b=="10"){
					$nmbln="Okt";
				} else if($b=="11"){
					$nmbln="Nov";
				} else if($b=="12"){
					$nmbln="Des";
				} else {
					$nmbln="";
				}
	return($nmbln);
}

?>