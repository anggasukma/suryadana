<?php
function tcpdf()
{
	 //require_once('tcpdf/config/lang/eng.php');
	 
	 /* php 7.2 */
	 require_once('TCPDF-master/tcpdf.php');
	 
	 /* php 7.0 */
    //require_once('tcpdf/tcpdf.php');
	
	
	/* untuk custom header */
	class TCPDF_Modif extends TCPDF {
		// Page header
		public function Header() {
			$headerData = $this->getHeaderData();
			$this->SetFont('times');
			$this->writeHTML($headerData['string']);
		}
	}
	/* end untuk custom header */
}

?>