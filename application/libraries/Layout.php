<?php
class Layout{
    protected $_ci;
    
    function __construct(){
        $this->_ci = &get_instance();
    }
    
    /*
    * View lengkap dengan sidebar,header,footer
    */
    function display($viewurl,$data=null){
        $data['_metadata']=$this->_ci->load->view('layouts/metadata',$data,true);
        $data['_header']=$this->_ci->load->view('layouts/header',$data,true);
        $data['_sidebar']=$this->_ci->load->view('layouts/sidebar',$data,true);
        $data['_content']=$this->_ci->load->view($viewurl,$data,true);
        $data['_footer']=$this->_ci->load->view('layouts/footer',$data,true);
        $data['_modal']=$this->_ci->load->view('layouts/modal',$data,true);
        $data['_notification']=$this->_ci->load->view('layouts/notification',$data,true);
        $data['_script']=$this->_ci->load->view('layouts/script',$data,true);
        $this->_ci->load->view('layouts/app.php',$data);
    }


    /*
    * View tanpa sidebar,header,footer
    */
    function displayblank($viewurl,$data=null){
        $data['_metadata']=$this->_ci->load->view('layouts/metadata',$data,true);
        $data['_notification']=$this->_ci->load->view('layouts/notification',$data,true);
        $data['_script']=$this->_ci->load->view('layouts/script',$data,true);
        $this->_ci->load->view($viewurl,$data);
    }
}
?>