<?php
class Mhelper extends CI_Model {
	
    function get_date(){
		$this->db->cache_off();
        $query_helper=$this->db->query("SELECT DATE_FORMAT(NOW(), '%y%m%d') AS datareturn");
        return $query_helper->result();
    }
    function get_date2(){
		$this->db->cache_off();
        $query_helper=$this->db->query("SELECT DATE_FORMAT(NOW(), '%Y-%m-%d') AS datareturn");
        return $query_helper->result();
    }
    function get_date2_mindate($min){
		$this->db->cache_off();
        $query_helper=$this->db->query("SELECT DATE_FORMAT((DATE_SUB(now(), INTERVAL ".$min." DAY)), '%Y-%m-%d') AS datareturn");
        return $query_helper->result();
    }
    function get_date2_plusdate($tglnow,$plus){
		$this->db->cache_off();
        $query_helper=$this->db->query("SELECT DATE_FORMAT((ADDDATE(".$tglnow.", INTERVAL ".$plus." DAY)), '%Y-%m-%d') AS datareturn");
        return $query_helper->result();
    }
    function get_date2_akhirdate($tgl){
		$this->db->cache_off();
        $query_helper=$this->db->query("SELECT DATE_FORMAT(LAST_DAY('".$tgl."'), '%Y-%m-%d') AS datareturn");
        return $query_helper->result();
    }
    function get_date3(){
		$this->db->cache_off();
        $query_helper=$this->db->query("SELECT DATE_FORMAT(NOW(), '%y%m') AS datareturn");
        return $query_helper->result();
    }
	
    function get_time(){
		$this->db->cache_off();
        $query_helper=$this->db->query("SELECT DATE_FORMAT(NOW(), '%H%i%s') AS datareturn");
        return $query_helper->result();
    }
    function get_time2(){
		$this->db->cache_off();
        $query_helper=$this->db->query("SELECT DATE_FORMAT(NOW(), '%H:%i:%s') AS datareturn");
        return $query_helper->result();
    }
	
    function get_datetime(){
		$this->db->cache_off();
        $query_helper=$this->db->query("SELECT DATE_FORMAT(NOW(), '%Y-%m-%d %H:%i:%s') AS datareturn");
        return $query_helper->result();
    }
	
    function get_datetime2(){
		$this->db->cache_off();
        $query_helper=$this->db->query("SELECT DATE_FORMAT(NOW(), '%Y%m%d%H%i%s') AS datareturn");
        return $query_helper->result();
    }
	
}
?>