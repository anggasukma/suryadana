<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mkegiatan extends CI_Model
{
	private $table = 'tb_kegiatan';
	private $table_unit = 'tb_unit';
	private $table_user = 'tb_user';
	private $table_alat = 'tb_alat';
	private $table_kegiatanalat = 'tb_kegiatanalat';


	/* untuk get data seluruhnya */
	public function getData(){
		return $this->db->order_by('tglkegiatan','ASC')->get($this->table)->result();
	}


	/* untuk get data by id */
	public function getDataByID($id){
		return $this->db->where('idkegiatan',$id)->get($this->table)->row();
	}


	/* untuk get data where array */
	public function getWhere($conditions){
		if(count($conditions)){
			return $this->db->join( $this->table_user , $this->table_user.'.iduser = '.$this->table.'.iduser_petugasit' , 'LEFT')->join( $this->table_unit , $this->table_unit.'.idunit = '.$this->table.'.idunit' , 'LEFT')->where($conditions)->order_by('namakegiatan')->get($this->table)->result();
		} else {
			return null;
		}		
	}
	public function getWhereRowsAlat($conditions){
		if(count($conditions)){
			return $this->db->where($conditions)->get($this->table_kegiatanalat)->num_rows();
		} else {
			return null;
		}		
	}


	/* untuk get data datatables */
	public function getListData($post, $mode = 'data'){
		$oder_column_index = $post['iSortCol_0'];
		$oder_column = $post['mDataProp_'.$oder_column_index];

		$this->db->query("SET @rownum := 0");
		$this->db->select('*,(@rownum  := @rownum  + 1) AS no');
		$this->db->join( $this->table_unit , $this->table_unit.'.idunit = '.$this->table.'.idunit' , 'LEFT');
		$this->db->join( $this->table_user , $this->table_user.'.iduser = '.$this->table.'.iduser_petugasit' , 'LEFT');

		if($post['tampilanmenu']=="kegiatansaya"){
			$this->db->where('iduser_petugasit',$post['iduser_petugasit']);
		}
		if($post['tglawal']!=""){
			$this->db->where('tglkegiatan>=',$post['tglawal']." 00:00:00");
		}
		if($post['tglakhir']!=""){
			$this->db->where('tglkegiatan<=',$post['tglakhir']." 23:59:59");
		}
		if($post['statuskegiatan']!=""){
			$this->db->where('statuskegiatan',$post['statuskegiatan']);
		}

		if($post['sSearch'] != ''){
			$query=$this->db->group_start();
			$query=$this->db->like('namakegiatan',$post['sSearch']);
			$query=$this->db->or_like('ketkegiatan',$post['sSearch']);
			$query=$this->db->group_end();
		}

		$this->db->order_by($oder_column,$post['sSortDir_0']);

		if($post['iDisplayLength'] > 0 && $mode == 'data'){
			$query=$this->db->limit($post['iDisplayLength'],$post['iDisplayStart']);
		}

		$query = $this->db->get($this->table);
		return $query->result_array();
	}
	public function getListAlat($post, $mode = 'data'){
		$oder_column_index = $post['iSortCol_0'];
		$oder_column = $post['mDataProp_'.$oder_column_index];

		$this->db->query("SET @rownum := 0");
		$this->db->select('*,(@rownum  := @rownum  + 1) AS no');
		$this->db->join( $this->table_alat , $this->table_alat.'.idalat = '.$this->table_kegiatanalat.'.idalat' , 'LEFT');
		$this->db->join( $this->table , $this->table.'.idkegiatan = '.$this->table_kegiatanalat.'.idkegiatan' , 'LEFT');

		$this->db->where($this->table_kegiatanalat.'.idkegiatan',$post['idkegiatan']);

		if($post['sSearch'] != ''){
			$query=$this->db->group_start();
			$query=$this->db->like('kodealat',$post['sSearch']);
			$query=$this->db->or_like('namaalat',$post['sSearch']);
			$query=$this->db->or_like('ketalat',$post['sSearch']);
			$query=$this->db->group_end();
		}

		$this->db->order_by($oder_column,$post['sSortDir_0']);

		if($post['iDisplayLength'] > 0 && $mode == 'data'){
			$query=$this->db->limit($post['iDisplayLength'],$post['iDisplayStart']);
		}

		$query = $this->db->get($this->table_kegiatanalat);
		return $query->result_array();
	}
	public function getListPAlat($post, $mode = 'data'){
		//untuk cek alat saat di kegiatan alat
		$oder_column_index = $post['iSortCol_0'];
		$oder_column = $post['mDataProp_'.$oder_column_index];

		$this->db->query("SET @rownum := 0");
		$this->db->select('*,(@rownum  := @rownum  + 1) AS no');
		$this->db->join( $this->table , $this->table.'.idkegiatan = '.$this->table_kegiatanalat.'.idkegiatan' , 'LEFT');

		$this->db->where('idalat',$post['idalat']);
		$this->db->where('tglkegiatan>=',$post['datekegiatan']." 00:00:00");
		$this->db->where('tglkegiatan<=',$post['datekegiatan']." 23:59:59");

		if($post['sSearch'] != ''){
			$query=$this->db->group_start();
			$query=$this->db->like('kodealat',$post['sSearch']);
			$query=$this->db->or_like('namaalat',$post['sSearch']);
			$query=$this->db->or_like('ketalat',$post['sSearch']);
			$query=$this->db->group_end();
		}

		$this->db->order_by($oder_column,$post['sSortDir_0']);

		if($post['iDisplayLength'] > 0 && $mode == 'data'){
			$query=$this->db->limit($post['iDisplayLength'],$post['iDisplayStart']);
		}

		$query = $this->db->get($this->table_kegiatanalat);
		return $query->result_array();
	}


	/* untuk store data */
	public function store($post){
		if($post['iduser_petugasit']==""){
			$post['iduser_petugasit']=null;
		}
		$this->db->query('ALTER TABLE '.$this->table.' AUTO_INCREMENT = 1;');
		return $this->db->insert($this->table,$post);
	}
	public function storealat($post){
		$this->db->query('ALTER TABLE '.$this->table_kegiatanalat.' AUTO_INCREMENT = 1;');
		return $this->db->insert($this->table_kegiatanalat,$post);
	}


	/* untuk update data */
	public function update($id,$post){
		if($post['iduser_petugasit']==""){
			$post['iduser_petugasit']=null;
		}
		$this->db->where('idkegiatan',$id);
		return $this->db->update($this->table,$post);
	}
	public function updateprosespengerjaan($id,$post){
		$this->db->where('idkegiatan',$id);
		return $this->db->update($this->table,$post);
	}
	public function updatealat($idkegiatanalat,$post){
		$this->db->where('idkegiatanalat',$idkegiatanalat);
		return $this->db->update($this->table_kegiatanalat,$post);
	}


	/* untuk hard delete data */
	public function delete($id){
		return $this->db->where('idkegiatan',$id)->delete($this->table);
	}
	public function deletealat($id){
		return $this->db->where('idkegiatanalat',$id)->delete($this->table_kegiatanalat);
	}


}