<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Munit extends CI_Model
{
	private $table = 'tb_unit';


	/* untuk get data seluruhnya */
	public function getData(){
		return $this->db->order_by('namaunit')->get($this->table)->result();
	}


	/* untuk get data by id */
	public function getDataByID($id){
		return $this->db->where('idunit',$id)->get($this->table)->row();
	}


	/* untuk get data where array */
	public function getWhere($conditions){
		if(count($conditions)){
			return $this->db->where($conditions)->order_by('namaunit')->get($this->table)->result();
		} else {
			return null;
		}		
	}


	/* untuk get data datatables */
	public function getListData($post, $mode = 'data'){
		$oder_column_index = $post['iSortCol_0'];
		$oder_column = $post['mDataProp_'.$oder_column_index];

		$this->db->query("SET @rownum := 0");
		$this->db->select('*,(@rownum  := @rownum  + 1) AS no');

		if($post['sSearch'] != ''){
			$query=$this->db->group_start();
			$query=$this->db->like('namaunit',$post['sSearch']);
			$query=$this->db->or_like('ketunit',$post['sSearch']);
			$query=$this->db->group_end();
		}

		$this->db->order_by($oder_column,$post['sSortDir_0']);

		if($post['iDisplayLength'] > 0 && $mode == 'data'){
			$query=$this->db->limit($post['iDisplayLength'],$post['iDisplayStart']);
		}

		$query = $this->db->get($this->table);
		return $query->result_array();
	}


	/* untuk store data */
	public function store($post){
		$this->db->query('ALTER TABLE '.$this->table.' AUTO_INCREMENT = 1;');
		return $this->db->insert($this->table,$post);
	}


	/* untuk update data */
	public function update($id,$post){
		$this->db->where('idunit',$id);
		return $this->db->update($this->table,$post);
	}


	/* untuk hard delete data */
	public function delete($id){
		return $this->db->where('idunit',$id)->delete($this->table);
	}


}