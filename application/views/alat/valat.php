<div class="">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">

			<!-- MODAL -->
        	<div class="x_panel">
        		<div class="x_title">
                	<h2>Alat</h2>
                  	<div class="nav navbar-right panel_toolbox">
	                    <button id="btn_add" class="btn btn-sm btn-dark"><i class="fa fa-plus"></i> Tambah Data</button>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                	
                	<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                		<thead>
                			<tr>
                				<th width="5%">No.</th>
                				<th width="20%">Kode Alat</th>
                				<th width="30%">Nama Alat</th>
                				<th>Keterangan</th>
                				<th width="10%">Aksi</th>
                			</tr>
                		</thead>
                		<tbody></tbody>
                	</table>

                </div>
			</div>
			<!-- / end MODAL -->

		</div>
    </div>


	<script type="text/javascript">
		$(document).ready(function(){
			var
			dTable=$('#datatable-responsive').dataTable({
				"bServerSide": true,
	            "bProcessing": true,
	            "sAjaxSource": "<?php echo app_path('alat/listdata');?>",
	            "sServerMethod": "POST",
	            "aoColumns": [
	                              { mData: 'no' } ,
	                              { mData: 'kodealat' } ,
	                              { mData: 'namaalat' } ,
	                              { mData: 'ketalat' } ,
	                              { mData: 'aksi', sClass: 'text-center' } ,
	                      ],
	      		bAutoWidth: false,
	      		"ScrollX": true,
	      		"sScrollX": "100%"
			});
			
			$("#btn_add").click(function(){
				window.location.href = "<?php echo app_path('alat/adddata');?>";
			});
		});

		function update(idalat){
			window.location.href = "<?php echo app_path('alat/updatedata');?>/"+idalat;
		}
		function del(idalat,kodealat){
			ketdel='<center>Kode Alat : <b>'+kodealat+'</b></center><form role="form" id="form_del" action="<?php echo app_path('alat/deldata_action');?>" method="post"><input type="hidden" class="form-control" id="idalat_del" name="idalat_del" value="'+idalat+'" readonly></form>';
			modal_open('Konfirmasi Hapus Data',ketdel,'sm','del')
		}
		// function delok(idalat){
		// 	window.location.href = "<?php echo app_path('alat/deldata_action');?>/"+idalat;
		// }
	</script>
</div>