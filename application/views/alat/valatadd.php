<div class="">
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2 class="cursorpointer" id="btn_back">Alat</h2> &nbsp; <h2><small><i class="fa fa-angle-double-right x_title_sub"></i> Tambah Data</small></h2>
				<div class="clearfix"></div>
			</div>
			
			<div class="x_content">
				 <form role="form" id="form_tambah" action="<?php echo app_path('alat/adddata_action');?>" method="post">
				 <center>
				 <div class="style_form1">
            <div class="form-group">
              <label>Kode Alat</label>
              <input type="text" class="form-control" id="kodealat" name="kodealat" maxlength="100" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label>Nama Alat</label>
              <input type="text" class="form-control" id="namaalat" name="namaalat" maxlength="255" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label>Keterangan</label>
              <textarea class="form-control" id="ketalat" name="ketalat"></textarea>
            </div>
            <div align="center">
            	<button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Simpan</button>
            </div>
				 </div>
				 </center>
				</form>
			</div>
		</div>
	</div>
</div>
</div>

<script>
	$("#form_tambah").validate({
		rules: {
			kodealat: {
				required: true,
				maxlength: 100,
				remote: {
					url: "<?php echo app_path('alat/check');?>",
					type: "post"
				}
			}
		}
	});

	$(document).ready(function() {
		$('#kodealat').focus();

		$("#btn_back").click(function(){
			window.location.href = "<?php echo app_path('alat');?>";
		});
	});
</script>