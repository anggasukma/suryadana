<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2 class="cursorpointer" id="btn_back">Alat</h2> &nbsp; <h2><small><i class="fa fa-angle-double-right x_title_sub"></i> Ubah Data</small></h2>
				<div class="clearfix"></div>
			</div>
			<?php
			$statusalat='';
			if($dataalat !=null) {
				foreach($dataalat as $r):
					$idalat=$r->idalat;
					$kodealat=$r->kodealat;
					$namaalat=$r->namaalat;
					$ketalat=$r->ketalat;
			?>
			<div class="x_content">
				 <form role="form" id="form_update" action="<?php echo app_path('alat/updatedata_action');?>" method="post">
				 <center>
				 <div class="style_form1">
					<div class="form-group hide">
						<label for="idalat">ID Alat</label>
						<input type="text" class="form-control" id="idalat" name="idalat" value="<?php echo $idalat; ?>" autocomplete="off" readonly>
					</div>
					<div class="form-group">
						<label>Kode Alat</label>
						<input type="text" class="form-control" id="kodealat" name="kodealat" value="<?php echo $kodealat; ?>" maxlength="100" required>
					</div>
					<div class="form-group">
						<label>Nama Alat</label>
						<input type="text" class="form-control" id="namaalat" name="namaalat" value="<?php echo $namaalat; ?>" maxlength="255" required>
					</div>
					<div class="form-group">
						<label>Keterangan</label>
						<textarea class="form-control" id="ketalat" name="ketalat"><?php echo $ketalat; ?></textarea>
					</div>
					<div align="center">
						<button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Simpan</button>
					</div>
				 </div>
				 </center>
             </form>
			</div>
			<?php
				endforeach;
			}
			?>
		</div>
	</div>
</div>

<script>
	$("#form_update").validate({
		rules: {
			kodealat: {
				required: true,
				maxlength: 100,
				remote: {
					url: "<?php echo app_path('alat/check');?>",
					type: "post",
					data: {
						idalat: function() {
							return $( "#idalat" ).val();
						}
					}
				}
			}
		}
	});
	
$(document).ready(function() {
	$('#kodealat').focus();

	$("#btn_back").click(function(){
			window.location.href = "<?php echo app_path('alat');?>";
		});
});
</script>