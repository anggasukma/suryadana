<!DOCTYPE html>
<html lang="en">
  <head>
    <?php echo $_metadata;?>
  
    <style type="text/css">
      .login {
          background: #081839 url("<?php echo base_url();?>assets/images/footer-bg-pattern.png");
          /* Center and scale the image nicely */
          background-repeat: no-repeat;
          background-size: cover;
      }
    </style>

  </head>

  <body class="login" >
    <!-- Page loader -->
    <div class="pageloader"></div>

    <div>
      <div class="login_wrapper">
        <div class="animate form login_form">

          <section class="login_content">
            <div class="x_panel">
              
              <form method="POST" name="form_login" id="form_login" action="<?php echo app_path('authenticate');?>">

                    <img src="<?php echo base_url('assets/images/logo.png');?>" width="100">
                <div>
                  <span>
                    <span style="font-size:24px;"><b ><?php echo app_title();?></b><br></span>
                    <i class="text-info"><?php echo app_titledashboard();?></i></span><br>
                  <p><span class="text-info">&nbsp;</span></p>
                </div>
                

                <div class="separator">
                  <div class="clearfix"><br></div>
                  <div class="item form-group">
                    <input type="text" class="form-control" id="username" name="username" placeholder="Username" autocomplete="off" required="" />
                  </div>
                  <div class="item form-group">
                    <input type="password" id="pass" class="form-control" placeholder="Password" required="" />
                    <input type="password" id="password" name="password" class="form-control" placeholder="Password" style="display:none;"/>
                  </div>
                  <div>
                    <input type="submit" class="btn btn-primary" value="Log in">
                  </div>

                </div>

                <div class="separator hide">                  
                  <div class="item form-group">
                    user : admin<br>
                    pass : admin
                  </div>
                </div>


                
              </form>

            </div>
          </section>

        </div>
      </div>
    </div>


    <script type="text/javascript">

    $("#form_login").validate({
      rules: {
        username: {required: true},
        pass: {required: true}
      }
    });

      $(document).ready(function(e) {
      $('#username').focus();

        $(document).on('keydown', function(e) {
          if(e.keyCode == 13){
            $('form').submit();
          }
        });

        $("#form_login").submit(function(e) {
          var
          pass=$('#pass').val();
          password=md5(pass);
          
          if(pass!=""){
            $('#password').val(password);
          }
        });

      });



      
    </script>


    <?php echo $_notification;?>
    <?php echo $_script;?>

  </body>
</html>
