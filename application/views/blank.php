<div class="">
	<div class="page-title">
    	<div class="title_left">
        	<h3>Check Page <small> Just add class <strong>menu_fixed</strong></small></h3>
		</div>
	</div>

	<div class="clearfix"></div>

	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">

			<!-- MODAL -->
        	<div class="x_panel">
            	<div class="x_title">
                	<h2>Modal</h2>
                  	<ul class="nav navbar-right panel_toolbox">
	                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
	                    <li class="dropdown">
	                    	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
	                        <ul class="dropdown-menu" role="menu">
	                          <li><a href="#">Settings 1</a>
	                          </li>
	                          <li><a href="#">Settings 2</a>
	                          </li>
	                        </ul>
	                    </li>
	                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                	
                	<button class="btn btn-default source" onclick="modal_open('Modal Large','content modal','lg','')">Modal Large</button>
                	<button class="btn btn-default source" onclick="modal_open('Modal Small','content modal','sm','')">Modal Small</button>

                </div>
			</div>
			<!-- / end MODAL -->

			<!-- NOTIF -->
        	<div class="x_panel">
            	<div class="x_title">
                	<h2>Notification</h2>
                  	<ul class="nav navbar-right panel_toolbox">
	                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
	                    <li class="dropdown">
	                    	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
	                        <ul class="dropdown-menu" role="menu">
	                          <li><a href="#">Settings 1</a>
	                          </li>
	                          <li><a href="#">Settings 2</a>
	                          </li>
	                        </ul>
	                    </li>
	                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                	
                	<button class="btn btn-info source" onclick="show_notif('Notif Info','text','info')">Notif Info</button>
                	<button class="btn btn-success source" onclick="show_notif('Notif Sukses','text','success')">Notif Sukses</button>
                	<button class="btn btn-warning source" onclick="show_notif('Notif Warning','text','warning')">Notif Warning</button>
                	<button class="btn btn-danger source" onclick="show_notif('Notif Error','text','error')">Notif Error</button>

                </div>
			</div>
			<!-- / end NOTIF -->





		</div>
    </div>


	<script type="text/javascript">
		$(document).ready(function(){
			// modal_open("tes","testing","sm","");
		})
	</script>
</div>