<div class="">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">

			<!-- MODAL -->
        	<div class="x_panel">
        		<div class="x_title">
					<?php
						$iduser_petugasit='';
						$tampilanmenu=$this->uri->segment(1);
						if($tampilanmenu=="kegiatansaya"){
							$iduser_petugasit=$this->session->userdata('jadwal_user_id');
					?>
						<h2>Kegiatan Saya</h2>
					<?php
					} else {
					?>
						<h2>Manajemen Kegiatan</h2>
					<?php
					}
					?>
                  	<div class="nav navbar-right panel_toolbox">
					  <?php if($this->session->userdata('jadwal_user_data')->hakakses=="Admin"){?>
	                    <button id="btn_add" class="btn btn-sm btn-dark"><i class="fa fa-plus"></i> Tambah Kegiatan</button>
					  <?php } ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
						<div style="padding-left: 0px;" class="form-group col-md-6 col-sm-12 col-xs-12">
							<div class="form-group col-md-3 col-sm-12 col-xs-12">
								Tanggal Kegiatan: 
							</div>
							<div class="form-group v_tglawal datepicker col-md-3 col-sm-12 col-xs-12" style="margin-right: 0px;">
							<input type="text" id="v_tglawal" value="<?php echo get_datetime('2');?>" class="form-control add-on" autocomplete="off" required>
							</div>
							<div class="form-group col-md-1 col-sm-12 col-xs-12" style="padding-left: 0px; width: 20px">
								s/d 
							</div>
							<div class="form-group v_tglakhir datepicker col-md-3 col-sm-12 col-xs-12" style="margin-right: 0px;">
							<input type="text" id="v_tglakhir" value="<?php echo get_datetime('2');?>" class="form-control add-on" autocomplete="off" required>
							</div>
						</div>
						<div class="form-group col-md-6 col-sm-12 col-xs-12">
							&nbsp;
						</div>
						<div class="clearfix"></div>
						<div  style="padding-left: 0px;"class="form-group col-md-6 col-sm-12 col-xs-12">
							<div class="form-group col-md-3 col-sm-12 col-xs-12" style="margin-left: 0px;">
								Status Kegiatan: 
							</div>
							<div class="form-group col-md-4 col-sm-12 col-xs-12" style="margin-right: 0px;">
								<select id="v_statuskegiatan" class="select2_single form-control">
									<?php echo get_selectoption_tb_kegiatan('statuskegiatan');?>
								</select>
							</div>
						</div>
						<div class="clearfix"></div>
						<hr>

                	<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                		<thead>
                			<tr>
                				<th width="5%">No.</th>
                				<th width="10%">Tanggal Kegiatan</th>
                				<th width="15%">Nama Kegiatan</th>
                				<th width="20%">Unit</th>
                				<th width="15%">Petugas IT</th>
                				<th>Keterangan</th>
                				<th width="10%">Aksi</th>
                			</tr>
                		</thead>
                		<tbody></tbody>
                	</table>

                </div>
			</div>
			<!-- / end MODAL -->

		</div>
    </div>


	<script type="text/javascript">
		$(document).ready(function(){
			var
			dTable=$('#datatable-responsive').dataTable({
				"bServerSide": true,
	            "bProcessing": true,
	            "sAjaxSource": "<?php echo app_path('kegiatan/listdata');?>",
				"fnServerParams": function ( aoData ) {
						aoData.push( { "name": "tglawal", "value": $('#v_tglawal').val() } , { "name": "tglakhir", "value": $('#v_tglakhir').val() } , { "name": "statuskegiatan", "value": $('#v_statuskegiatan').val() } , { "name": "iduser_petugasit", "value": "<?php echo $iduser_petugasit;?>" } , { "name": "tampilanmenu", "value": "<?php echo $tampilanmenu;?>" } );
					},
	            "sServerMethod": "POST",
	            "aoColumns": [
	                              { mData: 'no' } ,
	                              { mData: 'tglkegiatan_t' } ,
	                              { mData: 'namakegiatan' } ,
	                              { mData: 'namaunit' } ,
	                              { mData: 'namauser' } ,
	                              { mData: 'ketkegiatan' } ,
	                              { mData: 'aksi' } ,
	                      ],
	      		bAutoWidth: false,
	      		"ScrollX": true,
	      		"sScrollX": "100%"
			});

			$(".v_tglawal").on('changeDate', function(ev){
				dTable.api().ajax.reload();
			})
			$(".v_tglakhir").on('changeDate', function(ev){
				dTable.api().ajax.reload();
			})
			$('#v_statuskegiatan').change(function () {
				dTable.api().ajax.reload();
			});
			
			$("#btn_add").click(function(){
				window.location.href = "<?php echo app_path('kegiatan/adddata');?>";
			});
		});

		function update(idkegiatan){
			window.location.href = "<?php echo app_path('kegiatan/updatedata');?>/"+idkegiatan;
		}
		function alat(idkegiatan){
			var
			tampilanmenu='<?php echo $tampilanmenu; ?>';

			if(tampilanmenu=="kegiatansaya"){
				window.location.href = "<?php echo app_path('kegiatansaya/alat');?>/"+idkegiatan;
			} else {
				window.location.href = "<?php echo app_path('kegiatan/alat');?>/"+idkegiatan;
			}
		}
		function del(idkegiatan,namakegiatan){
			ketdel='<center>Nama Kegiatan : <b>'+namakegiatan+'</b></center><form role="form" id="form_del" action="<?php echo app_path('kegiatan/deldata_action');?>" method="post"><input type="hidden" class="form-control" id="idkegiatan_del" name="idkegiatan_del" value="'+idkegiatan+'" readonly></form>';
			modal_open('Konfirmasi Hapus Data',ketdel,'sm','del')
		}
		
		
		function prosespengerjaan(statuskegiatan,titletgledit,idkegiatan,namakegiatan){
			// alert (statuskegiatan);
			var
			statusalat='n';
			jmlalat=0;
			// if(statuskegiatan=="Selesai"){
				//cek keseluruhan barang sdh kembali atau belum
				$.ajax({
					type: "POST",
					url: "<?php echo app_path('kegiatan/cekbarangkembali');?>",
					data: "idkegiatan="+idkegiatan,
					// dataType:"json",
					cache: false,
					success: function(datareturn){
						jmlalat=datareturn;
						// alert(jmlalat);
						if(jmlalat==0){
							statusalat='y';
						}

						//tampilan modal
							ketdel='';
							ketdel+='<center>Kegiatan <b>"'+namakegiatan+'"</b> ke proses <b>"'+statuskegiatan+'"</b></center>';
							ketdel+='<form role="form" id="form_prosespengerjaan" action="<?php echo app_path($tampilanmenu.'/prosespengerjaan_action');?>" method="post">';
							ketdel+='<input type="hidden" class="form-control" name="idkegiatan" value="'+idkegiatan+'" readonly><hr>';
							ketdel+='<input type="hidden" class="form-control" name="statuskegiatan" value="'+statuskegiatan+'" readonly><hr>';
							ketdel+='<input type="hidden" class="form-control" name="titletgledit" value="'+titletgledit+'" readonly><hr>';
							// ketdel+='<center><div class="form-group"><label>Tanggal & Waktu</label><input style="max-width:160px;" type="text" class="form-control" name="'+titletgledit+'" value="<?php //echo get_datetime('5');?>" readonly></div></center><br>';
							ketdel+='<center><div class="form-group datepicker"><label>Tanggal Pinjam</label><input style="max-width:100px;" type="datetime" class="form-control add-on" value="<?php echo get_datetime('2');?>" name="datekegiatan" autocomplete="off" required></div></center>';
							ketdel+='<center><div class="form-group timepicker"><label>Waktu Pinjam</label><input style="max-width:80px;" type="text" class="form-control add-on" value="<?php echo get_datetime('4');?>" name="timekegiatan" autocomplete="off" required></div></center>';

							buttonsubmit='<center><button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> OK "'+statuskegiatan+'"</button></center>';
							if(statuskegiatan=="Selesai"){
								if(statusalat=='y'){
									ketdel+=buttonsubmit;
								} else {
									ketdel+='<center><font color="red"><b>ALAT YANG DIPINJAM MASIH ADA YANG BELUM DIKEMBALIKAN ('+jmlalat+').<br>HARAP ALAT DI CEK KEMBALI.</b></font></center>';
								}
							} else {
								ketdel+=buttonsubmit;
							}
							ketdel+='</form>';
							modal_open('Konfirmasi Proses Pengerjaan',ketdel,'sm','');
							$('#pinpinjam_pinjam').focus();
					},
					error: function() { alert("Error Cek Barang Kembali");}
				});
			// }
		}
	</script>
</div>