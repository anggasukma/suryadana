<div class="">
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<?php
				$tampilanmenu=$this->uri->segment(1);
				$act="";
				if($tampilanmenu=="kalender"){
					$act="kalender";
				?>
				?>
				<h2 class="cursorpointer" id="btn_back">Kalender Kegiatan</h2> &nbsp; <h2><small><i class="fa fa-angle-double-right x_title_sub"></i> Tambah Kegiatan</small></h2>
				<?php
				} else {
				?>
				<h2 class="cursorpointer" id="btn_back">Manajemen Kegiatan</h2> &nbsp; <h2><small><i class="fa fa-angle-double-right x_title_sub"></i> Tambah Kegiatan</small></h2>
				<?php
				}
				?>
				<div class="clearfix"></div>
			</div>
			
			<div class="x_content">
			<form role="form" id="form_tambah" action="<?php echo app_path('kegiatan/adddata_action/'.$act);?>" method="post">
			<center>
			<div class="style_form1">
            <div class="form-group datepicker">
              <label>Tanggal Kegiatan</label>
              <input type="datetime" class="form-control add-on" value="<?php echo $tglkegiatan;?>" id="datekegiatan" name="datekegiatan" autocomplete="off" required>
            </div>
            <div class="form-group timepicker">
              <label>Waktu Kegiatan</label>
              <input type="text" class="form-control add-on" value="00:00:00" id="timekegiatan" name="timekegiatan" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label>Nama Kegiatan</label>
              <input type="text" class="form-control" id="namakegiatan" name="namakegiatan" maxlength="255" autocomplete="off" required>
            </div>
			<div class="form-group">
              <label>Lokasi Kegiatan</label>
              <textarea class="form-control" id="lokasikegiatan" name="lokasikegiatan"></textarea>
            </div>
			<div class="form-group">
              <label>Unit</label>
              <select id="idunit" name="idunit" class="select2_single form-control" required>
				<?php 
				echo get_selectoption_tb_unit('data');
				?>
			  </select>
            </div>
            <div class="form-group">
              <label>PIC Unit Kegiatan</label>
              <input type="text" class="form-control" id="picunitkegiatan" name="picunitkegiatan" maxlength="255" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label>Keterangan Kegiatan</label>
              <textarea class="form-control" id="ketkegiatan" name="ketkegiatan"></textarea>
            </div>
			<div class="form-group">
              <label>Petugas IT</label>
              <select id="iduser_petugasit" name="iduser_petugasit" class="select2_single form-control">
				<?php 
				echo get_selectoption_tb_user('datapetugasit');
				?>
			  </select>
            </div>
            <div align="center">
            	<button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Simpan</button>
            </div>
				 </div>
				 </center>
				</form>
			</div>
		</div>
	</div>
</div>
</div>

<script>
	$(document).ready(function() {
		$('#namakegiatan').focus();

		$("#btn_back").click(function(){			
			var
			tampilanmenu='<?php echo $tampilanmenu; ?>';

			if(tampilanmenu=="kalender"){
				window.location.href = "<?php echo app_path('kalender');?>";
			} else {
				window.location.href = "<?php echo app_path('kegiatan');?>";
			}
		});
	});
</script>