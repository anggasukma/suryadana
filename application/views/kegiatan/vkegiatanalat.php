<style type="text/css">
div.modal-dialog {
  min-width: 70%;
}
</style>

<div class="">
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
					<?php
						$iduser_petugasit='';
						$tampilanmenu=$this->uri->segment(1);
						if($tampilanmenu=="kegiatansaya"){
							$iduser_petugasit=$this->session->userdata('jadwal_user_id');
					?>
						<h2 class="cursorpointer" id="btn_back">Kegiatan Saya</h2>
					<?php
					} else {
					?>
						<h2 class="cursorpointer" id="btn_back">Manajemen Kegiatan</h2>
					<?php
					}
					?>
				
				&nbsp; <h2><small><i class="fa fa-angle-double-right x_title_sub"></i> Alat</small></h2>
				<div class="clearfix"></div>
			</div>
			<?php
			$idkegiatan="";
			if($datakegiatan !=null) {
				foreach($datakegiatan as $r):
					$idkegiatan=$r->idkegiatan;
					$tglkegiatan=$r->tglkegiatan;
						$tglkegiatan_ex=explode(" ",$tglkegiatan);
						$datekegiatan=$tglkegiatan_ex[0];
						$timekegiatan=$tglkegiatan_ex[1];
					$namakegiatan=$r->namakegiatan;
					$lokasikegiatan=$r->lokasikegiatan;
					$idunit=$r->idunit;
					$namaunit=$r->namaunit;
					$picunitkegiatan=$r->picunitkegiatan;
					$ketkegiatan=$r->ketkegiatan;
					$iduser_petugasit=$r->iduser_petugasit;
					$namapetugasit=$r->namauser;
					$statuskegiatan=$r->statuskegiatan;
					$tglsedangdikerjakan=$r->tglsedangdikerjakan;
					$tglsudahdikerjakan=$r->tglsudahdikerjakan;
					$tglselesai=$r->tglselesai;
			?>
			<div class="x_content">
				<table width="100%">
					<tr><td width="200"><b>Tanggal & Waktu Kegiatan</b></td><td width="20">&nbsp; : &nbsp;</td><td><?php echo $tglkegiatan;?></td></tr>
					<tr><td><b>Nama Kegiatan</b></td><td>&nbsp; : &nbsp;</td><td><?php echo $namakegiatan;?></td></tr>
					<tr><td><b>Lokasi Kegiatan</b></td><td>&nbsp; : &nbsp;</td><td><?php echo $lokasikegiatan;?></td></tr>
					<tr><td><b>Unit Pelaksana Kegiatan</b></td><td>&nbsp; : &nbsp;</td><td><?php echo $namaunit;?></td></tr>
					<tr><td><b>PIC Unit Kegiatan</b></td><td>&nbsp; : &nbsp;</td><td><?php echo $picunitkegiatan;?></td></tr>
					<tr><td><b>Keterangan Kegiatan</b></td><td>&nbsp; : &nbsp;</td><td><?php echo $ketkegiatan;?></td></tr>
					<tr><td><b>Petugas IT</b></td><td>&nbsp; : &nbsp;</td><td><?php echo $namapetugasit;?></td></tr>
					<tr><td valign="top"><b>Status Kegiatan</b></td><td valign="top">&nbsp; : &nbsp;</td><td>
						<?php
						if($statuskegiatan=="Belum Dikerjakan"){
						?>
							<font color="<?php echo warna_statuskegiatan($statuskegiatan);?>"?><?php echo $statuskegiatan;?></font>
						<?php
						} else {
							if($statuskegiatan=="Sedang Dikerjakan"){
							?>
								<font color="<?php echo warna_statuskegiatan($statuskegiatan);?>"?><?php echo $statuskegiatan;?> [<?php echo $tglsedangdikerjakan;?>]</font><br>
							<?php
							} else {
							?>
								Sedang Dikerjakan [<?php echo $tglsedangdikerjakan;?>]<br>
							<?php
								if($statuskegiatan=="Sudah Dikerjakan"){
								?>
									<font color="<?php echo warna_statuskegiatan($statuskegiatan);?>"?><?php echo $statuskegiatan;?> [<?php echo $tglsudahdikerjakan;?>]</font><br>
								<?php
								} else {
								?>
									Sudah Dikerjakan [<?php echo $tglsudahdikerjakan;?>]<br>
								<?php
									if($statuskegiatan=="Selesai"){
									?>
										<font color="<?php echo warna_statuskegiatan($statuskegiatan);?>"?><?php echo $statuskegiatan;?> [<?php echo $tglselesai;?>]</font><br>
									<?php
									} else {
									?>
										Selesai [<?php echo $tglselesai;?>]<br>
									<?php
									}
								}
							}
						}
						?>
					</td></tr>
				</table>
			</div>
			<div class="x_title">
				<div class="clearfix"></div>
			</div>
			
			<div class="x_content">
			</div>

			<!-- MODAL -->
        	<div class="x_panel">
        		<div class="x_title">
					<?php if($this->session->userdata('jadwal_user_data')->hakakses=="Admin" && $statuskegiatan!="Selesai"){?>
                	<h2>Pilih alat yang dipinjam</h2>
					<?php } ?>
					
					<form role="form" id="form_tambah" action="<?php echo app_path('kegiatan/addalat_action');?>" method="post">
						<div class="form-group hide">
							<label for="idkegiatan">ID Kegiatan</label>
							<input type="text" class="form-control" id="idkegiatan" name="idkegiatan" value="<?php echo $idkegiatan; ?>" autocomplete="off" readonly>
						</div>
						<?php if($this->session->userdata('jadwal_user_data')->hakakses=="Admin" && $statuskegiatan!="Selesai"){?>
						 &nbsp; <select style="width:70%;" id="idalat" name="idalat" class="select2_single" required>
								<?php 
								echo get_selectoption_tb_alat('data');
								?>
							</select>
							<button type="submit" class="btn btn-dark btn-sm"><i class="fa fa-plus"></i> Tambah</button>
							<button type="button" onclick="cekalat();" class="btn btn-dark btn-sm"><i class="fa fa-search"></i> Cek</button>
						<?php } ?>
					</form>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                	
                	<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                		<thead>
                			<tr>
                				<th width="5%">No.</th>
                				<th width="15%">Kode Alat</th>
                				<th>Nama Alat</th>
                				<th width="10%">Tgl Pinjam</th>
                				<th width="10%">PIC Pinjam</th>
                				<th width="10%">Tgl Kembali</th>
                				<th width="10%">PIC Kembali</th>
                				<th width="5%">Aksi</th>
                			</tr>
                		</thead>
                		<tbody></tbody>
                	</table>

                </div>
			</div>
			<!-- / end MODAL -->

			<?php
				endforeach;
			}
			?>
		</div>
	</div>
</div>
</div>

<script>
	$(document).ready(function(){
			var
			dTable=$('#datatable-responsive').dataTable({
				"bServerSide": true,
	            "bProcessing": true,
	            "sAjaxSource": "<?php echo app_path('kegiatan/listalat');?>",
				"fnServerParams": function ( aoData ) {
						aoData.push( { "name": "idkegiatan", "value": <?php echo $idkegiatan;?> } );
					},
	            "sServerMethod": "POST",
	            "aoColumns": [
	                              { mData: 'no' } ,
	                              { mData: 'kodealat' } ,
	                              { mData: 'namaalat' } ,
	                              { mData: 'tglpinjam' } ,
	                              { mData: 'picpinjam' } ,
	                              { mData: 'tglkembali' } ,
	                              { mData: 'pickembali' } ,
	                              { mData: 'aksi', sClass: 'text-right' } ,
	                      ],
	      		bAutoWidth: false,
	      		"ScrollX": true,
	      		"sScrollX": "100%"
			});
			
			$("#btn_add").click(function(){
				window.location.href = "<?php echo app_path('kegiatan/adddata');?>";
			});
		});


	$(document).ready(function() {
		$('#namakegiatan').focus();
		$('#idunit').val('<?php echo $idunit; ?>').change();
		$('#iduser_petugasit').val('<?php echo $iduser_petugasit; ?>').change();

		$("#btn_back").click(function(){
			var
			tampilanmenu='<?php echo $tampilanmenu; ?>';

			if(tampilanmenu=="kegiatansaya"){
				window.location.href = "<?php echo app_path('kegiatansaya');?>";
			} else {
				window.location.href = "<?php echo app_path('kegiatan');?>";
			}
			
		});
	});

	function pinjam(idkegiatan,idkegiatanalat,namaalat,kodealat){
		ketdel='';
		ketdel+='<center>Kode / Nama Alat : <b>'+kodealat+' / '+namaalat+'</b></center>';
		ketdel+='<form role="form" id="form_pinjam" action="<?php echo app_path($tampilanmenu.'/pinjamalat_action');?>" method="post">';
		ketdel+='<input type="hidden" class="form-control" id="idkegiatanalat_pinjam" name="idkegiatanalat" value="'+idkegiatanalat+'" readonly>';
		ketdel+='<input type="hidden" class="form-control" id="idkegiatan_pinjam" name="idkegiatan" value="'+idkegiatan+'" readonly><hr>';
		ketdel+='<center><div class="form-group datepicker"><label>Tanggal Pinjam</label><input style="max-width:100px;" type="datetime" class="form-control add-on" value="<?php echo get_datetime('2');?>" id="datepinjam_pinjam" name="datepinjam" autocomplete="off" required></div></center>';
		ketdel+='<center><div class="form-group timepicker"><label>Waktu Pinjam</label><input style="max-width:80px;" type="text" class="form-control add-on" value="<?php echo get_datetime('4');?>" id="timepinjam_pinjam" name="timepinjam" autocomplete="off" required></div></center>';
		ketdel+='<center><div class="form-group"><label>PIC Pinjam (Petugas Yang Mengambil Alat)</label><input style="max-width:300px;" type="text" class="form-control" id="picpinjam_pinjam" name="picpinjam" maxlength="255" autocomplete="off" required></div></center>';
		ketdel+='<center><button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Pinjam/Ambil Alat</button></center>';
		ketdel+='</form>';
		modal_open('Konfirmasi Pinjam Alat',ketdel,'sm','');
		$('#pinpinjam_pinjam').focus();
	}
	function kembali(idkegiatan,idkegiatanalat,namaalat,kodealat){
		ketdel='';
		ketdel+='<center>Kode / Nama Alat : <b>'+kodealat+' / '+namaalat+'</b></center>';
		ketdel+='<form role="form" id="form_kembali" action="<?php echo app_path($tampilanmenu.'/kembalialat_action');?>" method="post">';
		ketdel+='<input type="hidden" class="form-control" id="idkegiatanalat_kembali" name="idkegiatanalat" value="'+idkegiatanalat+'" readonly>';
		ketdel+='<input type="hidden" class="form-control" id="idkegiatan_kembali" name="idkegiatan" value="'+idkegiatan+'" readonly><hr>';
		ketdel+='<center><div class="form-group datepicker"><label>Tanggal Kembali</label><input style="max-width:100px;" type="datetime" class="form-control add-on" value="<?php echo get_datetime('2');?>" id="datekembali_kembali" name="datekembali" autocomplete="off" required></div></center>';
		ketdel+='<center><div class="form-group timepicker"><label>Waktu Kembali</label><input style="max-width:80px;" type="text" class="form-control add-on" value="<?php echo get_datetime('4');?>" id="timekembali_kembali" name="timekembali" autocomplete="off" required></div></center>';
		ketdel+='<center><div class="form-group"><label>PIC Kembali (Petugas Yang Mengembalikan Alat)</label><input style="max-width:300px;" type="text" class="form-control" id="pickembali_kembali" name="pickembali" maxlength="255" autocomplete="off" required></div></center>';
		ketdel+='<center><button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Alat Kembali</button></center>';
		ketdel+='</form>';
		modal_open('Konfirmasi Pengembalian Alat',ketdel,'sm','');
		$('#pinpinjam_pinjam').focus();
	}

	function delalat(idkegiatan,idkegiatanalat,namaalat){
		ketdel='<center>Nama Alat : <b>'+namaalat+'</b></center><form role="form" id="form_del" action="<?php echo app_path('kegiatan/delalat_action');?>" method="post"><input type="hidden" class="form-control" id="idkegiatanalat_del" name="idkegiatanalat_del" value="'+idkegiatanalat+'" readonly><input type="hidden" class="form-control" id="idkegiatan_del" name="idkegiatan_del" value="'+idkegiatan+'" readonly></form>';
		modal_open('Konfirmasi Hapus Alat',ketdel,'sm','del')
	}
	function cekalat(){
		var
		idalat=$('#idalat').val();
		if(idalat!=""){
			isi='';
			isi+='<table id="datapeminjamanalat" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%"><thead><tr><th width="5%">No.</th><th width="20%">Tgl & Waktu</th><th width="40%">Nama Kegiatan</th><th>Keterangan Kegiatan</th></tr></thead><tbody></tbody></table>';
			modal_open('Cek Peminjaman Alat',isi,'sm');
		}
			dTablealat=$('#datapeminjamanalat').dataTable({
				"bServerSide": true,
	            "bProcessing": true,
	            "sAjaxSource": "<?php echo app_path('kegiatan/listpeminjamanalat');?>",
				"fnServerParams": function ( aoData ) {
						aoData.push( { "name": "datekegiatan", "value": '<?php echo $datekegiatan;?>' } , { "name": "idalat", "value": $('#idalat').val() } );
					},
	            "sServerMethod": "POST",
	            "aoColumns": [
	                              { mData: 'no' } ,
	                              { mData: 'tglkegiatan' } ,
	                              { mData: 'namakegiatan' } ,
	                              { mData: 'ketkegiatan' } ,
	                      ],
	      		bAutoWidth: false,
	      		"ScrollX": true,
	      		"sScrollX": "100%"
			});
			// $('#idalat').change(function () {
			// 	dTablealat.api().ajax.reload();
			// 	// alert("reload");
			// });
	}
</script>