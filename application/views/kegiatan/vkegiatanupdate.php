<div class="">
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2 class="cursorpointer" id="btn_back">Manajemen Kegiatan</h2> &nbsp; <h2><small><i class="fa fa-angle-double-right x_title_sub"></i> Ubah Kegiatan</small></h2>
				<div class="clearfix"></div>
			</div>
			<?php
			if($datakegiatan !=null) {
				foreach($datakegiatan as $r):
					$idkegiatan=$r->idkegiatan;
					$tglkegiatan=$r->tglkegiatan;
						$tglkegiatan_ex=explode(" ",$tglkegiatan);
						$datekegiatan=$tglkegiatan_ex[0];
						$timekegiatan=$tglkegiatan_ex[1];
					$namakegiatan=$r->namakegiatan;
					$lokasikegiatan=$r->lokasikegiatan;
					$idunit=$r->idunit;
					$picunitkegiatan=$r->picunitkegiatan;
					$ketkegiatan=$r->ketkegiatan;
					$iduser_petugasit=$r->iduser_petugasit;
					$statuskegiatan=$r->statuskegiatan;
			?>
			<div class="x_content">
			<form role="form" id="form_tambah" action="<?php echo app_path('kegiatan/updatedata_action');?>" method="post">
			<center>
			<div class="style_form1">
			<div class="form-group hide">
				<label for="idkegiatan">ID Kegiatan</label>
				<input type="text" class="form-control" id="idkegiatan" name="idkegiatan" value="<?php echo $idkegiatan; ?>" autocomplete="off" readonly>
			</div>
            <div class="form-group datepicker">
              <label>Tanggal Kegiatan</label>
              <input type="datetime" class="form-control add-on" value="<?php echo $datekegiatan;?>" id="datekegiatan" name="datekegiatan" autocomplete="off" required>
            </div>
            <div class="form-group timepicker">
              <label>Waktu Kegiatan</label>
              <input type="text" class="form-control add-on" value="<?php echo $timekegiatan;?>" id="timekegiatan" name="timekegiatan" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label>Nama Kegiatan</label>
              <input type="text" class="form-control" id="namakegiatan" name="namakegiatan" value="<?php echo $namakegiatan; ?>" maxlength="255" autocomplete="off" required>
            </div>
			<div class="form-group">
              <label>Lokasi Kegiatan</label>
              <textarea class="form-control" id="lokasikegiatan" name="lokasikegiatan"><?php echo $lokasikegiatan; ?></textarea>
            </div>
			<div class="form-group">
              <label>Unit</label>
              <select id="idunit" name="idunit" class="select2_single form-control" required>
				<?php 
				echo get_selectoption_tb_unit('data');
				?>
			  </select>
            </div>
            <div class="form-group">
              <label>PIC Unit Kegiatan</label>
              <input type="text" class="form-control" id="picunitkegiatan" name="picunitkegiatan" value="<?php echo $picunitkegiatan; ?>" maxlength="255" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label>Keterangan Kegiatan</label>
              <textarea class="form-control" id="ketkegiatan" name="ketkegiatan"><?php echo $ketkegiatan; ?></textarea>
            </div>
			<div class="form-group">
              <label>Petugas IT</label>
              <select id="iduser_petugasit" name="iduser_petugasit" class="select2_single form-control">
				<?php 
				echo get_selectoption_tb_user('datapetugasit');
				?>
			  </select>
            </div>
            <div align="center">
            	<button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Simpan</button>
            </div>
				 </div>
				 </center>
				</form>
			</div>
			<?php
				endforeach;
			}
			?>
		</div>
	</div>
</div>
</div>

<script>
	$(document).ready(function() {
		$('#namakegiatan').focus();
		$('#idunit').val('<?php echo $idunit; ?>').change();
		$('#iduser_petugasit').val('<?php echo $iduser_petugasit; ?>').change();

		$("#btn_back").click(function(){
			window.location.href = "<?php echo app_path('kegiatan');?>";
		});
	});
</script>