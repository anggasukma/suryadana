<!DOCTYPE html>
<html lang="en">
  <head>
    <?php echo $_metadata; ?>
  </head>

  <body class="nav-md">
    <!-- Page loader -->
    <div class="pageloader"></div>

    <div class="container body">
      <div class="main_container">

        <!-- SIDEBAR -->
        <?php echo $_sidebar; ?>
        <!-- / end SIDEBAR -->


        <!-- top navigation -->
        <?php echo $_header; ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main" style="min-height: 686px;">
        <?php echo $_content; ?>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php echo $_footer; ?>
        <!-- /footer content -->

      </div>
    </div>
    
    <!-- jQuery custom content scroller -->
    <script src="<?php echo base_url('assets/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js');?>"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url('assets/build/js/custom.min.js');?>"></script>

    <!-- modal template -->
    <?php echo $_modal; ?>
    <!-- /modal template -->

    <!-- notification template -->
    <?php echo $_notification; ?>
    <!-- /notification template -->

    <!-- global script -->
    <?php echo $_script; ?>
    <!-- /global script -->

  </body>
</html>