<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- MAP -->
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&amp;sensor=false"></script>

<!-- Bootstrap -->
<link href="<?php echo base_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css');?>" rel="stylesheet">
<!-- Font Awesome -->
<link href="<?php echo base_url('assets/vendors/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet">
<!-- NProgress -->
<link href="<?php echo base_url('assets/vendors/nprogress/nprogress.css');?>" rel="stylesheet">
<!-- iCheck -->
<link href="<?php echo base_url('assets/vendors/iCheck/skins/flat/green.css');?>" rel="stylesheet">
<!-- jQuery custom content scroller -->
<link href="<?php echo base_url('assets/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css');?>" rel="stylesheet"/>

<!-- jQuery -->
<script src="<?php echo base_url('assets/vendors/jquery/dist/jquery.min.js');?>"></script>
<!-- Datatables -->
<link href="<?php echo base_url('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css');?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css');?>" rel="stylesheet">

<!-- Select2 -->
    <link href="<?php echo base_url('assets/vendors/select2/dist/css/select2.min.css');?>" rel="stylesheet">


<!-- jquery.inputmask -->
    <script src="<?php echo base_url('assets/vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js');?>"></script>
    <script src="<?php echo base_url('assets/vendors/jquery.inputmask/dist/min/inputmask/inputmask.numeric.extensions.min.js');?>"></script>

<!-- bootstrap-daterangepicker -->
    <link href="<?php echo base_url('assets/vendors/bootstrap-daterangepicker/daterangepicker.css');?>" rel="stylesheet">

<!-- datetimepicker -->
	<link href="<?php echo base_url('assets/bootstrap-datetimepicker-0.0.11/css/bootstrap-datetimepicker.min.css');?>" rel="stylesheet" />

<!-- PNotify -->
	<link href="<?php echo base_url('assets/vendors/pnotify/dist/pnotify.css');?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/vendors/pnotify/dist/pnotify.buttons.css');?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/vendors/pnotify/dist/pnotify.nonblock.css');?>" rel="stylesheet">

<!-- Custom Theme Style -->
<link href="<?php echo base_url('assets/build/css/custom.css');?>" rel="stylesheet">

<!-- Bootstrap -->
<script src="<?php echo base_url('assets/vendors/bootstrap/dist/js/bootstrap.min.js');?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url('assets/vendors/fastclick/lib/fastclick.js');?>"></script>
<!-- NProgress -->
<script src="<?php echo base_url('assets/vendors/nprogress/nprogress.js');?>"></script>

<!-- iCheck -->
<script src="<?php echo base_url('assets/vendors/iCheck/icheck.min.js');?>"></script>
<!-- Switchery -->
<script src="<?php echo base_url('assets/vendors/switchery/dist/switchery.min.js');?>"></script>

<!-- PNotify -->
    <script src="<?php echo base_url('assets/vendors/pnotify/dist/pnotify.js');?>"></script>
    <script src="<?php echo base_url('assets/vendors/pnotify/dist/pnotify.buttons.js');?>"></script>
    <script src="<?php echo base_url('assets/vendors/pnotify/dist/pnotify.nonblock.js');?>"></script>

<!-- validator 
    <script src="<?php //base_url();?>assets/vendors/validator/validator.js"></script>-->

<!-- md5 -->
    <script src="<?php echo base_url('assets/md5.js');?>"></script>

<!-- Number Format -->
    <script src="<?php echo base_url('assets/number_format.js');?>"></script>

<!-- Validation -->
	<script src="<?php echo base_url('assets/jquery-validation-bootstrap-tooltip-master/jquery.validate.js');?>"></script>
	<script src="<?php echo base_url('assets/jquery-validation-bootstrap-tooltip-master/jquery-validate.bootstrap-tooltip.js');?>"></script>

<!-- Datatables -->
    <script src="<?php echo base_url('assets/vendors/datatables.net/js/jquery.dataTables.min.js');?>"></script>
    <script src="<?php echo base_url('assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js');?>"></script>
    <script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js');?>"></script>
    <script src="<?php echo base_url('assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js');?>"></script>
    <script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.flash.min.js');?>"></script>
    <script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.html5.min.js');?>"></script>
    <script src="<?php echo base_url('assets/vendors/datatables.net-buttons/js/buttons.print.min.js');?>"></script>
    <script src="<?php echo base_url('assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js');?>"></script>
    <script src="<?php echo base_url('assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js');?>"></script>
    <script src="<?php echo base_url('assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js');?>"></script>
    <script src="<?php echo base_url('assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js');?>"></script>
    <script src="<?php echo base_url('assets/vendors/datatables.net-scroller/js/datatables.scroller.min.js');?>"></script>
    <script src="<?php echo base_url('assets/vendors/jszip/dist/jszip.min.js');?>"></script>
    <script src="<?php echo base_url('assets/vendors/pdfmake/build/pdfmake.min.js');?>"></script>
    <script src="<?php echo base_url('assets/vendors/pdfmake/build/vfs_fonts.js');?>"></script>

<!-- Select2 -->
    <script src="<?php echo base_url('assets/vendors/select2/dist/js/select2.full.min.js');?>"></script>

<!-- bootstrap-daterangepicker -->
    <script src="<?php echo base_url('assets/vendors/moment/min/moment.min.js');?>"></script>
    <script src="<?php echo base_url('assets/vendors/bootstrap-daterangepicker/daterangepicker.js');?>"></script>

<!-- datetimepicker -->
	<script src="<?php echo base_url('assets/bootstrap-datetimepicker-0.0.11/js/bootstrap-datetimepicker.js');?>"></script>

<style type="text/css">
	/* Paste this css to your style sheet file or under head tag */
    /* This only works with JavaScript, 
    if it's not present, don't show loader */
    .no-js #loader { display: none;  }
    .js #loader { display: block; position: absolute; left: 100px; top: 0; }
    .pageloader {
    	position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url("<?php echo base_url('assets/images/loader-64x/Preloader_10.gif'); ?>" ) center no-repeat #fff;
	}

    .cursorpointer{
        cursor:pointer;
    }
	.style_form1 {
		max-width:500px; 
		text-align: left;
	}

    .bg_batal{
        background-color:black;
    }

    .left_col {
        background: #081839;
        background-image: url("<?php echo base_url('assets/images/footer-bg-pattern.png'); ?>");
    }
    .nav_title {
        background: #081839;
    }
</style>


    <title> <?php echo app_title();?> </title>
	<link rel="icon" href="<?php echo base_url("assets/images/".app_logo());?>">