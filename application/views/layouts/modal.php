<!-- Modal Large -->
<div class="modal fade" id="modal_lg" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button> -->
        <h4 class="modal-title">Default Modal</h4>
      </div>

      <div class="modal-body">
       
      </div>
      <div class="modal-footer">
        
      </div>
      
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- / Modal Large -->


<!-- Modal Small -->
<div class="modal fade" id="modal_sm" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button> -->
        <h4 class="modal-title">Default Modal</h4>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- / Modal Small -->


<script>
function modal_open(title,content,type,content_type = ''){
  $("#modal_"+type+" .overlay").show('slow');
  $("#modal_"+type).find(".modal-title").html(title);
  

  switch (content_type) {
		case "del":
      $("#modal_"+type).find(".modal-body").html('');
      $("#modal_"+type).find(".modal-body").html('<center>Yakin hapus data?<br></center>');
      $("#modal_"+type).find(".modal-body").append(content);
      $("#modal_"+type).find(".modal-footer").html('');
      $("#modal_"+type).find(".modal-footer").html('<button onclick="FungsiSubmitDel();" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Ok</button><button onclick="modal_close(\'sm\');" class="btn btn-sm"><i class="fa fa-times"></i> Batal</button>');
		break;
		case "simpannota":
      $("#modal_"+type).find(".modal-body").html('');
      $("#modal_"+type).find(".modal-body").html('<center>Yakin simpan nota?<br>Nota yang telah disimpan tidak dapat dihapus.</center>');
      $("#modal_"+type).find(".modal-body").append(content);
      $("#modal_"+type).find(".modal-footer").html('');
      $("#modal_"+type).find(".modal-footer").html('<button onclick="FungsiSimpanNotaOk();" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Ok</button><button onclick="modal_close(\'sm\');" class="btn btn-sm"><i class="fa fa-times"></i> Batal</button>');
		break;
		case "hapusnota":
      $("#modal_"+type).find(".modal-body").html('');
      $("#modal_"+type).find(".modal-body").html('<center>Yakin hapus nota ?</center>');
      $("#modal_"+type).find(".modal-body").append(content);
      $("#modal_"+type).find(".modal-footer").html('');
      $("#modal_"+type).find(".modal-footer").html('<button onclick="FungsiSubmitDel();" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Ok</button><button onclick="modal_close(\'sm\');" class="btn btn-sm"><i class="fa fa-times"></i> Batal</button>');
		break;
		case "selesainota":
      $("#modal_"+type).find(".modal-body").html('');
      $("#modal_"+type).find(".modal-body").html('<center>Yakin selesaikan nota dan update stok ?<br>Nota yang telah selasai tidak dapat diubah atau dihapus.</center>');
      $("#modal_"+type).find(".modal-body").append(content);
      $("#modal_"+type).find(".modal-footer").html('');
      $("#modal_"+type).find(".modal-footer").html('<button onclick="FungsiSubmitSelesai();" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Ok</button><button onclick="modal_close(\'sm\');" class="btn btn-sm"><i class="fa fa-times"></i> Batal</button>');
		break;
		case "proses":
      $("#modal_"+type).find(".modal-body").html('');
      $("#modal_"+type).find(".modal-body").append(content);
      $("#modal_"+type).find(".modal-footer").html('');
      $("#modal_"+type).find(".modal-footer").html('<button onclick="FungsiProses();" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Ok</button><button onclick="modal_close(\'sm\');" class="btn btn-sm"><i class="fa fa-times"></i> Batal</button>');
		break;
    default :
      $("#modal_"+type).find(".modal-body").html('');
      $("#modal_"+type).find(".modal-body").append(content);
      $("#modal_"+type).find(".modal-footer").html('<button onclick="modal_close(\'sm\');" class="btn btn-sm"><i class="fa fa-times"></i> Kembali</button>');
  }
  
  // if(content_type == 'del'){
	// $("#modal_"+type).find(".modal-body").html('');
	// $("#modal_"+type).find(".modal-body").html('<center>Yakin hapus data?<br></center>');
	// $("#modal_"+type).find(".modal-body").append(content);
	// $("#modal_"+type).find(".modal-footer").html('');
	// $("#modal_"+type).find(".modal-footer").html('<button onclick="FungsiSubmitDel();" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Ok</button><button onclick="modal_close(\'sm\');" class="btn btn-sm"><i class="fa fa-times"></i> Batal</button>');
  // } else {
	// $("#modal_"+type).find(".modal-body").load(content);
  // }

  $(function() {   
		$(".datepicker").datetimepicker({
			pickTime: false,
			format: 'yyyy-MM-dd'
		});

		$(".timepicker").datetimepicker({
			pickDate: false,
			format: 'hh:mm:ss'
		});
	});
  
  $("#modal_"+type).modal("show");
  $("#modal_"+type+" .overlay").show('hide');
}

function modal_close(type){
  $("#modal_"+type).modal("hide");
}

function FungsiSubmitDel(){
  $("#form_del").submit();
}
</script>
