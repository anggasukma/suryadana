<!-- Notification Template  -->

<?php
/* --- Notif by flash session --- */

  # info #
  $show_info = false;
  if($this->session->flashdata('info')){
    $show_info = true;
  }

  # success #
  $show_success = false;
  if($this->session->flashdata('success')){
    $show_success = true;
  }

  # warning #
  $show_warning = false;
  if($this->session->flashdata('warning')){
    $show_warning = true;
  }

  # error #
  $show_error = false;
  if($this->session->flashdata('error')){
    $show_error = true;
  }

/* --- // END Notif by flash session --- */
?>
<script type="text/javascript">
  $(document).ready(function(){

     <?php if($show_info){ ?>
        show_notif('Info','<?php echo $this->session->flashdata('info');?>','info');
     <?php } ?>

     <?php if($show_success){ ?>
        show_notif('Sukses','<?php echo $this->session->flashdata('success');?>','success');
     <?php } ?>

     <?php if($show_warning){ ?>
        show_notif('Peringatan','<?php echo $this->session->flashdata('warning');?>','warning');
     <?php } ?>

     <?php if($show_error){ ?>
        show_notif('Error','<?php echo $this->session->flashdata('error');?>','error');
     <?php } ?>


  });
</script>

<script type="text/javascript">
/* --- by function --- */
  
  function show_notif(title,text,type){
    new PNotify({
      title: title,
      text: text,
      type: type,
      styling: 'bootstrap3'
    });
  }

/* --- END by function --- */
</script>

<!-- / Notification Template --> 