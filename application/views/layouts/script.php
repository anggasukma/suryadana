<script type="text/javascript">
	
	//paste this code under head tag or in a seperate js file.
    // Wait for window load
    $(window).on("load",function() {
    	// Animate loader off screen
        setTimeout(function(){ $(".pageloader").fadeOut("slow"); }, 1000);
	});

	function logout(){
		var c = confirm('Yakin ingin keluar dari sistem ?');
		if(c){
			location.href = '<?php echo app_path('authenticate/logout'); ?>';
		}
	}
	
	$(document).ready(function() {
		$(".select2_single").select2();
		$(":input").inputmask();

		$('[data-toggle="tooltip"]').tooltip();  
	});
	
    Inputmask.extendAliases({
        'rupiah': {
            alias: "numeric",
            prefix: "",
            groupSeparator: ",",
            placeholder: "0",
            autoGroup: !0,
            digits: 0,
            digitsOptional: !4,
            clearMaskOnLostFocus: !4
        }
    });
	
	$(function() {   
		$(".datepicker").datetimepicker({
			pickTime: false,
			format: 'yyyy-MM-dd'
		});

		$(".timepicker").datetimepicker({
			pickDate: false,
			format: 'hh:mm:ss'
		});
	});

	// Remove the formatting to get integer data for summation
	var intVal = function ( i ) {
		return typeof i === 'string' ? i.replace(/[\$,]/g, '')*1 : typeof i === 'number' ?	i : 0;
	};

	$.fn.clearValidation = function(){var v = $(this).validate();$('[name]',this).each(function(){v.successList.push(this);v.showErrors();});v.resetForm();v.reset();};

	function get_date2_plusdate(tgl,plus){
		var date = new Date(tgl);
   			var newdate = new Date(date);

				newdate.setDate(newdate.getDate() + plus);
    
			var dd = newdate.getDate();
				if(dd<10){
					dd='0'+dd;
				}
			var mm = newdate.getMonth() + 1;
				if(mm<10){
					mm='0'+mm;
				}
			var y = newdate.getFullYear();

			var someFormattedDate = y + '-' + mm + '-' + dd;
			return someFormattedDate;
			// alert(someFormattedDate);
	}

	//hanya angka dan alphabets
	function allowAlphaNumericSpace(event) {
		var code = ('charCode' in event) ? event.charCode : event.keyCode;
		if (!(code == 32) && // space
			!(code > 47 && code < 58) && // numeric (0-9)
			!(code > 64 && code < 91) && // upper alpha (A-Z)
			!(code > 96 && code < 123) && // lower alpha (a-z)
			!(code == 44 ) && // ,
			!(code == 45 ) && // -
			!(code == 47 ) && // /
			!(code == 40 ) && // (
			!(code == 41 ) && // )
			!(code == 91 ) && // [
			!(code == 93 ) && // ]
			!(code == 46 ) // .
		) { 
			event.preventDefault();
		}
	}
	$(document).ready(function(){
		$('input').keypress(allowAlphaNumericSpace);
	});	
	// document.input.addEventListener('input', function(e) {
	// 	const allowedCharacters="0123456789azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN "; // You can add any other character in the same way
	// 	let newValue="";
	// 	this.value.split("").forEach(function(char){
	// 		if(in_array(char, allowedCharacters.split(""))) newValue+=char;
	// 	});
	// 	this.value=newValue;
	// });
	// function in_array(elem, array){
	// 	let isIn=false;
	// 	for(var i=0;i<array.length;i++){
	// 		if(elem==array[i]) isIn=true;
	// 	}
	// 	return isIn;
	// }
</script>