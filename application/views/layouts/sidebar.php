    <div class="col-md-3 left_col menu_fixed">
          <div class="left_col scroll-view">

            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url(); ?>" class="site_title"><span style="font-size:20px;"><img src="<?php echo base_url('assets/images/logo.png');?>" width="50"> &nbsp;<b ><?php echo app_title();?></b></span>
                    <small class="text-info"><img src="<?php echo base_url('assets/images/logo.png');?>" width="50"></small></span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <!-- <div class="profile">
              <div class="profile_pic">
                <img src="<?php //echo base_url('assets/images/user.png');?>" class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php //echo $this->session->jadwal_user_data->namauser;?></h2>
              </div>
            </div> -->
            <!-- /menu profile quick info -->

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

              <div class="menu_section">
              <hr>

              <?php
              switch ($this->session->jadwal_user_data->hakakses) {
                case "Admin":
              ?>
                <ul class="nav side-menu">
                  <li class="mn_user"><a href="<?php echo app_path('user');?>"><i class="fa fa-user"></i> Pengguna</a></li>
                  <li class="mn_unit mn_alat"><a><i class="fa fa-folder"></i> Parameter <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li class="mn_unit"><a href="<?php echo app_path('unit');?>"> Unit</a></li>
                      <li class="mn_alat"><a href="<?php echo app_path('alat');?>"> Alat</a></li>
                    </ul>
                  </li>
                  <li class="mn_kegiatan"><a href="<?php echo app_path('kegiatan');?>"><i class="fa fa-plus"></i> Manajemen Kegiatan</a></li>
                  <li class="mn_kalender"><a href="<?php echo app_path('kalender');?>"><i class="fa fa-calendar"></i> Kalender Kegiatan</a></li>
                  <li class="mn_pengaturan"><a href="<?php echo app_path('pengaturan');?>"><i class="fa fa-cogs"></i> Pengaturan</a></li>
                </ul>
              <?php
                break;
                case "Petugas IT":
              ?>
                <ul class="nav side-menu">
                  <li class="mn_kegiatan"><a href="<?php echo app_path('kegiatan');?>"><i class="fa fa-list"></i> Manajemen Kegiatan</a></li>
                  <li class="mn_kegiatansaya"><a href="<?php echo app_path('kegiatansaya');?>"><i class="fa fa-star"></i> Kegiatan Saya</a></li>
                  <li class="mn_kalender"><a href="<?php echo app_path('kalender');?>"><i class="fa fa-calendar"></i> Kalender Kegiatan</a></li>
                </ul>
              <?php
                break;
              }
              ?>


              </div>


              <!-- <div class="menu_section">
                <h3>Live On</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-bug"></i> Additional Pages <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="e_commerce.html">E-commerce</a></li>
                      <li><a href="projects.html">Projects</a></li>
                      <li><a href="project_detail.html">Project Detail</a></li>
                      <li><a href="contacts.html">Contacts</a></li>
                      <li><a href="profile.html">Profile</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-windows"></i> Extras <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="page_403.html">403 Error</a></li>
                      <li><a href="page_404.html">404 Error</a></li>
                      <li><a href="page_500.html">500 Error</a></li>
                      <li><a href="plain_page.html">Plain Page</a></li>
                      <li><a href="login.html">Login Page</a></li>
                      <li><a href="pricing_tables.html">Pricing Tables</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-sitemap"></i> Multilevel Menu <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="#level1_1">Level One</a>
                        <li><a>Level One<span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                            <li class="sub_menu"><a href="level2.html">Level Two</a>
                            </li>
                            <li><a href="#level2_1">Level Two</a>
                            </li>
                            <li><a href="#level2_2">Level Two</a>
                            </li>
                          </ul>
                        </li>
                        <li><a href="#level1_2">Level One</a>
                        </li>
                    </ul>
                  </li>                  
                  <li><a href="javascript:void(0)"><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a></li>
                </ul>
              </div> -->

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <!-- <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Support">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
            </div> -->
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- SIDE BAR ACTIVE -->
	<?php
		$get_controller=strtolower($this->uri->segment(app_controller()));
			echo "
				<script>
				$('.mn_".$get_controller."').addClass('active');
				$('.mn_".$get_controller." > ul').attr('style','display: block;');;
				</script>
				";
	?>