<div class="">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">

			<!-- MODAL -->
        	<div class="x_panel">
        		<div class="x_title">
                	<h2>Pengaturan</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
               
                	<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                		<thead>
                			<tr>
                				<th width="5%">No.</th>
                				<th width="30%">Data</th>
                				<th width="55%">Isi</th>
                				<th width="10%">Aksi</th>
                			</tr>
                		</thead>
                		<tbody></tbody>
                	</table>

                </div>
			</div>
			<!-- / end MODAL -->

		</div>
    </div>


	<script type="text/javascript">
		$(document).ready(function(){
			var
			dTable=$('#datatable-responsive').dataTable({
				"bServerSide": true,
	            "bProcessing": true,
	            "sAjaxSource": "<?php echo app_path('pengaturan/listdata');?>",
	            "sServerMethod": "POST",
	            "aoColumns": [
	                              { mData: 'no' } ,
	                              { mData: 'namadata' } ,
	                              { mData: 'isidata' } ,
	                              { mData: 'aksi', sClass: 'text-center' } ,
	                      ],
	      		bAutoWidth: false,
	      		"ScrollX": true,
	      		"order": [[ 1, "asc" ]],
	      		"sScrollX": "100%"
			});

		});

		function update(iddata){
			window.location.href = "<?php echo app_path('pengaturan/updatedata');?>/"+iddata;
		}
	</script>
</div>