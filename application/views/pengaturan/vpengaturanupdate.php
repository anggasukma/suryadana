<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2 class="cursorpointer" id="btn_back">Pengaturan</h2> &nbsp; <h2><small><i class="fa fa-angle-double-right x_title_sub"></i> Ubah Data</small></h2>
				<div class="clearfix"></div>
			</div>
			<?php
			if($datapengaturan !=null) {
				foreach($datapengaturan as $r):
					$iddata=$r->iddata;
					$namadata=$r->namadata;
					$isidata=$r->isidata;
			?>
			<div class="x_content">
				 <form role="form" id="form_update" action="<?php echo app_path('pengaturan/updatedata_action');?>" method="post">
				 <center>
				 <div class="style_form1">
					<div class="form-group hide">
						<label for="iddata">ID Data</label>
						<input type="text" class="form-control" id="iddata" name="iddata" value="<?php echo $iddata; ?>" readonly>
					</div>
					<div class="form-group">
						<label>Nama</label>
						<textarea class="form-control" id="isidata" rows="3" name="namadata" readonly><?php echo $namadata; ?></textarea>
					</div>
					<div class="form-group">
						<label>Isi</label>
						<textarea class="form-control" id="isidata" rows="5" name="isidata"><?php echo $isidata; ?></textarea>
					</div>
					<div align="center">
						<button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Simpan</button>
					</div>
				 </div>
				 </center>
             </form>
			</div>
			<?php
				endforeach;
			}
			?>
		</div>
	</div>
</div>

<script>
	$("#form_update").validate({
		
	});
	
$(document).ready(function() {
	$('#namadata').focus();

	$("#btn_back").click(function(){
			window.location.href = "<?php echo app_path('pengaturan');?>";
		});
});
</script>