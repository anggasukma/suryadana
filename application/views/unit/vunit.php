<div class="">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">

			<!-- MODAL -->
        	<div class="x_panel">
        		<div class="x_title">
                	<h2>Unit</h2>
                  	<div class="nav navbar-right panel_toolbox">
	                    <button id="btn_add" class="btn btn-sm btn-dark"><i class="fa fa-plus"></i> Tambah Data</button>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                	
                	<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                		<thead>
                			<tr>
                				<th width="5%">No.</th>
                				<th width="30%">Nama</th>
                				<th>Keterangan</th>
                				<th width="10%">Aksi</th>
                			</tr>
                		</thead>
                		<tbody></tbody>
                	</table>

                </div>
			</div>
			<!-- / end MODAL -->

		</div>
    </div>


	<script type="text/javascript">
		$(document).ready(function(){
			var
			dTable=$('#datatable-responsive').dataTable({
				"bServerSide": true,
	            "bProcessing": true,
	            "sAjaxSource": "<?php echo app_path('unit/listdata');?>",
	            "sServerMethod": "POST",
	            "aoColumns": [
	                              { mData: 'no' } ,
	                              { mData: 'namaunit' } ,
	                              { mData: 'ketunit' } ,
	                              { mData: 'aksi', sClass: 'text-center' } ,
	                      ],
	      		bAutoWidth: false,
	      		"ScrollX": true,
	      		"sScrollX": "100%"
			});
			
			$("#btn_add").click(function(){
				window.location.href = "<?php echo app_path('unit/adddata');?>";
			});
		});

		function update(idunit){
			window.location.href = "<?php echo app_path('unit/updatedata');?>/"+idunit;
		}
		function del(idunit,namaunit){
			ketdel='<center>Nama Unit : <b>'+namaunit+'</b></center><form role="form" id="form_del" action="<?php echo app_path('unit/deldata_action');?>" method="post"><input type="hidden" class="form-control" id="idunit_del" name="idunit_del" value="'+idunit+'" readonly></form>';
			modal_open('Konfirmasi Hapus Data',ketdel,'sm','del')
		}
		// function delok(idunit){
		// 	window.location.href = "<?php echo app_path('unit/deldata_action');?>/"+idunit;
		// }
	</script>
</div>