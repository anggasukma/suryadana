<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2 class="cursorpointer" id="btn_back">Unit</h2> &nbsp; <h2><small><i class="fa fa-angle-double-right x_title_sub"></i> Ubah Data</small></h2>
				<div class="clearfix"></div>
			</div>
			<?php
			$statusunit='';
			if($dataunit !=null) {
				foreach($dataunit as $r):
					$idunit=$r->idunit;
					$namaunit=$r->namaunit;
					$ketunit=$r->ketunit;
			?>
			<div class="x_content">
				 <form role="form" id="form_update" action="<?php echo app_path('unit/updatedata_action');?>" method="post">
				 <center>
				 <div class="style_form1">
					<div class="form-group hide">
						<label for="idunit">ID Unit</label>
						<input type="text" class="form-control" id="idunit" name="idunit" value="<?php echo $idunit; ?>" autocomplete="off" readonly>
					</div>
					<div class="form-group">
						<label>Nama Unit</label>
						<input type="text" class="form-control" id="namaunit" name="namaunit" value="<?php echo $namaunit; ?>" maxlength="255" required>
					</div>
					<div class="form-group">
						<label>Keterangan</label>
						<textarea class="form-control" id="ketunit" name="ketunit"><?php echo $ketunit; ?></textarea>
					</div>
					<div align="center">
						<button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Simpan</button>
					</div>
				 </div>
				 </center>
             </form>
			</div>
			<?php
				endforeach;
			}
			?>
		</div>
	</div>
</div>

<script>
	$("#form_update").validate({
		rules: {
			namaunit: {
				required: true,
				maxlength: 255,
				remote: {
					url: "<?php echo app_path('unit/check');?>",
					type: "post",
					data: {
						idunit: function() {
							return $( "#idunit" ).val();
						}
					}
				}
			}
		}
	});
	
$(document).ready(function() {
	$('#namaunit').focus();

	$("#btn_back").click(function(){
			window.location.href = "<?php echo app_path('unit');?>";
		});
});
</script>