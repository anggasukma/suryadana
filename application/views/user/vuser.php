<div class="">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">

			<!-- MODAL -->
        	<div class="x_panel">
        		<div class="x_title">
                	<h2>Pengguna</h2>
                  	<div class="nav navbar-right panel_toolbox">
	                    <button id="btn_add" class="btn btn-sm btn-dark"><i class="fa fa-plus"></i> Tambah Data</button>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                	
						<div class="form-group col-md-3 col-sm-12 col-xs-12 pull-right" style="max-width:237px;">
							<div class="form-group col-md-12 col-sm-12 col-xs-12" style="max-width:50px;">
								Status: 
							</div>
							<div class="form-group col-md-9 col-sm-12 col-xs-12" style="margin-right: 0px;">
								<select id="v_statususer" class="select2_single form-control">
									<?php echo get_selectoption_tb_user('statususer');?>
								</select>
							</div>
						</div>
                	<table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                		<thead>
                			<tr>
                				<th width="5%">No.</th>
                				<th width="30%">Nama</th>
                				<th width="35%">Username</th>
                				<th width="20%">Hak Akses</th>
                				<th width="10%">Aksi</th>
                			</tr>
                		</thead>
                		<tbody></tbody>
                	</table>

                </div>
			</div>
			<!-- / end MODAL -->

		</div>
    </div>


	<script type="text/javascript">
		$(document).ready(function(){
			var
			dTable = $('#datatable-responsive').dataTable({
				"bServerSide": true,
	            "bProcessing": true,
	            "sAjaxSource": "<?php echo app_path('user/listdata');?>",
					"fnServerParams": function ( aoData ) {
						aoData.push( { "name": "statususer", "value": $('#v_statususer').val() } );
					},
	            "sServerMethod": "POST",
	            "aoColumns": [
	                              { mData: 'no' } ,
	                              { mData: 'namauser' } ,
	                              { mData: 'username' } ,
	                              { mData: 'hakakses' } ,
	                              { mData: 'aksi', sClass: 'text-center' } ,
	                      ],
	      		bAutoWidth: false,
	      		"ScrollX": true,
	      		"sScrollX": "100%"
			});

			$('#v_statususer').change(function () {
				dTable.api().ajax.reload();
			});

			$("#btn_add").click(function(){
				window.location.href = "<?php echo app_path('user/adddata');?>";
			});
		});
		function update(isuser){
			window.location.href = "<?php echo app_path('user/updatedata');?>/"+isuser;
		}
		function del(isuser,username){
			ketdel='<center>Username : <b>'+username+'</b></center><form role="form" id="form_del" action="<?php echo app_path('user/deldata_action');?>" method="post"><input type="hidden" class="form-control" id="iduser_del" name="iduser_del" value="'+isuser+'" readonly></form>';
			modal_open('Konfirmasi Hapus Data',ketdel,'sm','del')
		}
		// function delok(isuser){
		// 	window.location.href = "<?php echo app_path('user/deldata_action');?>/"+isuser;
		// }
	</script>
</div>