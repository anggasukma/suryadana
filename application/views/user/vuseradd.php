<div class="">
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2 class="cursorpointer" id="btn_back">Pengguna</h2> &nbsp; <h2><small><i class="fa fa-angle-double-right x_title_sub"></i> Tambah Data</small></h2>
				<div class="clearfix"></div>
			</div>
			
			<div class="x_content">
				 <form role="form" id="form_tambah" action="<?php echo app_path('user/adddata_action');?>" method="post">
				 <center>
				 <div class="style_form1">
            <div class="form-group">
              <label>Nama</label>
              <input type="text" class="form-control" id="namauser" name="namauser" maxlength="100" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label>Username</label>
              <input type="text" class="form-control" id="username" name="username" maxlength="50" autocomplete="off" required>
            </div>
            <div class="form-group">
              <label>Password</label>
              <input type="password" class="form-control" id="password" name="password" maxlength="50" required>
            </div>
            <div class="form-group">
              <label>Ulangi Password</label>
              <input type="password" class="form-control" id="passwordulang" maxlength="50" required>
            </div>
            <div class="form-group">
              <label>Hak Akses</label>
              <select id="hakakses" name="hakakses" class="select2_single form-control" required>
								<?php echo get_selectoption_tb_user('hakakses');?>
					   	</select>
            </div>
            <div class="form-group">
              <label>Status Pengguna</label>
              <select id="statususer" name="statususer" class="select2_single form-control" required>
								<?php echo get_selectoption_tb_user('statususer');?>
					   	</select>
            </div>
            <div align="center">
            	<button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Simpan</button>
            </div>
				 </div>
				 </center>
				</form>
			</div>
		</div>
	</div>
</div>
</div>

<script>
	$("#form_tambah").validate({
		rules: {
			namauser: {
				required: true,
				maxlength: 100
			},
			username: {
				required: true,
				maxlength: 50,
				remote: {
					url: "<?php echo app_path('user/check');?>",
					type: "post"
				}
			},
			password: {
				required: true
			},
			passwordulang: {
				required: true
			}
		}
	});
	
	$("#form_tambah").submit(function(e) {
          var
          password=$('#password').val();
          passwordulang=$('#passwordulang').val();
          passworden=md5(password);
          
					if($("#form_tambah").valid()==true){
						if(password==passwordulang){
							$('#password').val(passworden);
							$('#passwordulang').val(passworden);
						} else {
							e.preventDefault();
							$('#password').val("");
							$('#passwordulang').val("");
							$("#form_tambah").valid();
							$('#password').focus();
						}
					}
	});

	$(document).ready(function() {
		$('#namauser').focus();

		$("#btn_back").click(function(){
			window.location.href = "<?php echo app_path('user');?>";
		});
	});
</script>