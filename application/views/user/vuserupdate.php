<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2 class="cursorpointer" id="btn_back">Pengguna</h2> &nbsp; <h2><small><i class="fa fa-angle-double-right x_title_sub"></i> Ubah Data</small></h2>
				<div class="clearfix"></div>
			</div>
			<?php
			$hakakses='';
			$statususer='';
			if($datauser !=null) {
				foreach($datauser as $r):
					$iduser=$r->iduser;
					$namauser=$r->namauser;
					$username=$r->username;
					$hakakses=$r->hakakses;
					$statususer=$r->statususer;
			?>
			<div class="x_content">
				 <form role="form" id="form_update" action="<?php echo app_path('user/updatedata_action');?>" method="post">
				 <center>
				 <div class="style_form1">
					<div class="form-group hide">
						<label for="iduser">ID User</label>
						<input type="text" class="form-control" id="iduser" name="iduser" value="<?php echo $iduser; ?>" readonly>
					</div>
					<div class="form-group">
						<label>Nama</label>
						<input type="text" class="form-control" id="namauser" name="namauser" value="<?php echo $namauser; ?>" maxlength="100" required>
					</div>
					<div class="form-group">
						<label>Username</label>
						<input type="text" class="form-control" id="username" value="<?php echo $username; ?>" maxlength="50" readonly required>
					</div>
					<div class="form-group">
						<label>Password</label>
						<input type="password" class="form-control" id="password" name="password" placeholder="isi bila ingin mengubah password" maxlength="50">
					</div>
					<div class="form-group">
						<label>Ulangi Password</label>
						<input type="password" class="form-control" id="passwordulang" placeholder="isi bila ingin mengubah password" maxlength="50">
					</div>
					<div class="form-group">
						<label>Hak Akses</label>
						<select id="hakakses" name="hakakses" class="select2_single form-control" required>
							<?php echo get_selectoption_tb_user('hakakses');?>
						</select>
					</div>
					<div class="form-group">
						<label>Status Pengguna</label>
						<select id="statususer" name="statususer" class="select2_single form-control" required>
							<?php echo get_selectoption_tb_user('statususer');?>
						</select>
					</div>
					<div align="center">
						<button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Simpan</button>
					</div>
				 </div>
				 </center>
             </form>
			</div>
			<?php
				endforeach;
			}
			?>
		</div>
	</div>
</div>

<script>
	$("#form_update").validate({
		rules: {
			namauser: {
				required: true,
				maxlength: 100
			},
			username: {
				required: true,
				maxlength: 50,
				remote: {
					url: "<?php echo app_path('user/check');?>",
					type: "post",
					data: {
						iduser: function() {
							return $( "#iduser" ).val();
						}
					}
				}
			}
		}
	});

	$("#form_update").submit(function(e) {
		var
		password=$('#password').val();
		passwordulang=$('#passwordulang').val();
		passworden=md5(password);
		
				if($("#form_update").valid()==true){
					if(password!="" || passwordulang!=""){
						if(password==passwordulang){
							$('#password').val(passworden);
							$('#passwordulang').val(passworden);
						} else {
							e.preventDefault();
							$('#password').val("");
							$('#passwordulang').val("");
							$("#form_update").valid();
							$('#password').focus();
						}
					}
				}
	});
	
$(document).ready(function() {
	$('#namauser').focus();
	$('#hakakses').val('<?php echo $hakakses;?>').change();
	$('#statususer').val('<?php echo $statususer;?>').change();

	$("#btn_back").click(function(){
			window.location.href = "<?php echo app_path('user');?>";
		});
});
</script>