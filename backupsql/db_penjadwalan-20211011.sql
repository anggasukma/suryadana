-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 11, 2021 at 04:33 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_penjadwalan`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_alat`
--

CREATE TABLE `tb_alat` (
  `idalat` int(11) NOT NULL,
  `kodealat` varchar(100) NOT NULL,
  `namaalat` varchar(255) NOT NULL,
  `ketalat` text NOT NULL,
  `updatedb` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_alat`
--

INSERT INTO `tb_alat` (`idalat`, `kodealat`, `namaalat`, `ketalat`, `updatedb`) VALUES
(1, 'Laptop1', 'Laptop Asus Seri Blabla', 'Bla Bla', '2021-10-11 14:24:06'),
(2, 'Laptop2', 'Laptop Lenovo', '', '2021-10-11 14:24:40'),
(3, 'Pointer1', 'Pointer Asus', 'Warna Biru Muda', '2021-10-11 14:25:05');

-- --------------------------------------------------------

--
-- Table structure for table `tb_data`
--

CREATE TABLE `tb_data` (
  `iddata` int(11) NOT NULL,
  `namadata` varchar(225) NOT NULL,
  `isidata` text NOT NULL,
  `update_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_data`
--

INSERT INTO `tb_data` (`iddata`, `namadata`, `isidata`, `update_at`) VALUES
(1, 'Unit Kerja', 'RSUP Sanglah', '2021-10-11 12:51:38'),
(2, 'Nama Aplikasi', 'PENCATATAN JADWAL TUGAS INSTALASI IT', '2021-10-11 12:55:44');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kegiatan`
--

CREATE TABLE `tb_kegiatan` (
  `idkegiatan` int(11) NOT NULL,
  `tglkegiatan` datetime NOT NULL,
  `namakegiatan` varchar(255) NOT NULL,
  `lokasikegiatan` text NOT NULL,
  `idunit` int(11) NOT NULL COMMENT 'unit yg melakukan kegiatan',
  `picunitkegiatan` varchar(255) NOT NULL COMMENT 'pic dari unit yg melakukan kegiatan',
  `ketkegiatan` text NOT NULL,
  `iduser_admin` int(11) DEFAULT NULL,
  `iduser_petugasit` int(11) DEFAULT NULL,
  `statuskegiatan` enum('Belum Dikerjakan','Sedang Dikerjakan','Sudah Dikerjakan','Selesai') NOT NULL DEFAULT 'Belum Dikerjakan',
  `tglinput` datetime NOT NULL,
  `tglsedangdikerjakan` datetime NOT NULL,
  `tglsudahdikerjakan` datetime NOT NULL,
  `tglselesai` datetime NOT NULL,
  `updatedb` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kegiatanalat`
--

CREATE TABLE `tb_kegiatanalat` (
  `idkegiatanalat` int(11) NOT NULL,
  `idkegiatan` int(11) NOT NULL,
  `idalat` int(11) NOT NULL,
  `tglpinjam` datetime NOT NULL,
  `picpinjam` varchar(255) NOT NULL,
  `tglkembali` datetime NOT NULL,
  `pickembali` varchar(255) NOT NULL,
  `updatedb` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_unit`
--

CREATE TABLE `tb_unit` (
  `idunit` int(11) NOT NULL,
  `namaunit` varchar(255) NOT NULL,
  `ketunit` text NOT NULL,
  `updatedb` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_unit`
--

INSERT INTO `tb_unit` (`idunit`, `namaunit`, `ketunit`, `updatedb`) VALUES
(1, 'IBS', 'Instalasi Bedah Central', '2021-10-11 14:12:06'),
(2, 'Poli Mata', '', '2021-10-11 14:14:02');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `iduser` int(11) NOT NULL,
  `namauser` varchar(100) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` char(32) NOT NULL,
  `hakakses` enum('Admin','Petugas IT') DEFAULT NULL,
  `statususer` enum('0','1') NOT NULL DEFAULT '0',
  `create_at` datetime NOT NULL,
  `updatedb` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`iduser`, `namauser`, `username`, `password`, `hakakses`, `statususer`, `create_at`, `updatedb`) VALUES
(1, 'Admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Admin', '1', '2021-10-11 11:32:15', '2021-10-11 03:32:15'),
(2, 'Made Suryadana', 'surya', 'aff8fbcbf1363cd7edc85a1e11391173', 'Petugas IT', '1', '2021-10-11 20:50:00', '2021-10-11 12:50:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_alat`
--
ALTER TABLE `tb_alat`
  ADD PRIMARY KEY (`idalat`),
  ADD UNIQUE KEY `kodealat` (`kodealat`);

--
-- Indexes for table `tb_data`
--
ALTER TABLE `tb_data`
  ADD PRIMARY KEY (`iddata`);

--
-- Indexes for table `tb_kegiatan`
--
ALTER TABLE `tb_kegiatan`
  ADD PRIMARY KEY (`idkegiatan`),
  ADD KEY `idunit` (`idunit`),
  ADD KEY `iduser_admin` (`iduser_admin`),
  ADD KEY `iduser_petugasit` (`iduser_petugasit`);

--
-- Indexes for table `tb_kegiatanalat`
--
ALTER TABLE `tb_kegiatanalat`
  ADD PRIMARY KEY (`idkegiatanalat`),
  ADD UNIQUE KEY `idkegiatan_2` (`idkegiatan`,`idalat`),
  ADD KEY `idkegiatan` (`idkegiatan`),
  ADD KEY `idalat` (`idalat`);

--
-- Indexes for table `tb_unit`
--
ALTER TABLE `tb_unit`
  ADD PRIMARY KEY (`idunit`),
  ADD UNIQUE KEY `namaunit` (`namaunit`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`iduser`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_alat`
--
ALTER TABLE `tb_alat`
  MODIFY `idalat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_data`
--
ALTER TABLE `tb_data`
  MODIFY `iddata` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tb_kegiatan`
--
ALTER TABLE `tb_kegiatan`
  MODIFY `idkegiatan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_kegiatanalat`
--
ALTER TABLE `tb_kegiatanalat`
  MODIFY `idkegiatanalat` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_unit`
--
ALTER TABLE `tb_unit`
  MODIFY `idunit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_kegiatan`
--
ALTER TABLE `tb_kegiatan`
  ADD CONSTRAINT `tb_kegiatan_ibfk_1` FOREIGN KEY (`idunit`) REFERENCES `tb_unit` (`idunit`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_kegiatan_ibfk_2` FOREIGN KEY (`iduser_admin`) REFERENCES `tb_user` (`iduser`) ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_kegiatan_ibfk_3` FOREIGN KEY (`iduser_petugasit`) REFERENCES `tb_user` (`iduser`) ON UPDATE CASCADE;

--
-- Constraints for table `tb_kegiatanalat`
--
ALTER TABLE `tb_kegiatanalat`
  ADD CONSTRAINT `tb_kegiatanalat_ibfk_1` FOREIGN KEY (`idkegiatan`) REFERENCES `tb_kegiatan` (`idkegiatan`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tb_kegiatanalat_ibfk_2` FOREIGN KEY (`idalat`) REFERENCES `tb_alat` (`idalat`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
